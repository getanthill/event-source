module.exports = {
  collectCoverageFrom: ['src/**/*.{js,jsx,ts}', '!**/node_modules/**'],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100,
    },
  },
  globalSetup: '<rootDir>/test/setup.ts',
  globalTeardown: '<rootDir>/test/teardown.ts',
  transform: {
    '^.+\\.(ts|js)$': 'babel-jest',
  },
};
