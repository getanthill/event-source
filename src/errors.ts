/**
 * EventSourced errors
 */
export const INCOMPATIBLE_EVENT_VERSION_WITH_STATE_VERSION = new Error(
  'An event must not have a version before its reduction',
);
export const HANDLER_RETRY_TIMEOUT = new Error('Retry duration reached');
export const HANDLER_IMPERATIVE_CONDITION_FAILED = new Error(
  'Imperative condition failed',
);
export const ENTITY_IS_READONLY = new Error('Entity is readonly');
