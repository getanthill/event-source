import { strict as assert } from 'assert';
import * as crypto from 'crypto';

import { cloneDeep, omit } from 'lodash';

import { deepFreeze } from './utils/deep-freeze';

import { Events } from './Events';
import { Snapshots } from './Snapshots';
import * as ERRORS from './errors';

import { Db } from 'mongodb';

import * as date from './utils/date';

interface Logger {
  info: Function;
  warn: Function;
  error: Function;
}

export interface Options {
  CORRELATION_FIELD: string;
  CREATED_AT_FIELD: string;
  CURRENT_HASH_FIELD: string;
  PREVIOUS_HASH_FIELD: string;
  NONCE_FIELD: string;
  EVENTS_COLLECTION_NAME: string;
  SNAPSHOTS_COLLECTION_NAME: string;
  WITH_GLOBAL_VERSION: boolean;
  WITH_BLOCKCHAIN_HASH: boolean;
  BLOCKCHAIN_HASH_DIFFICULTY: number;
  BLOCKCHAIN_HASH_GENESIS: string;
}

/**
 * Factory of the EventSourced class
 * @param {string} collectionName Name of the main state collection
 * @param {function} reducer The reducer to apply
 * @param {object} options Additional options
 * @param {string} [options.CORRELATION_FIELD='correlation_id'] The correlation id
 * @param {string} [options.CREATED_AT_FIELD='created_at'] The event created_at field
 * @param {string} [options.CURRENT_HASH_FIELD='hash'] The blockchain hash field
 * @param {string} [options.PREVIOUS_HASH_FIELD='prev'] The blockchain previous hash field
 * @param {string} [options.NONCE_FIELD='nonce'] The blockchain hash field
 * @param {string} [options.EVENTS_COLLECTION_NAME=`${collectionName}_events`]
 *  The events collection name
 * @param {string} [options.SNAPSHOTS_COLLECTION_NAME=`${collectionName}_snapshots`]
 * @param {boolean} [options.WITH_GLOBAL_VERSION=false]
 *  Does the model be initialized with a global version instead
 *  of a entity-local one
 * @param {number} [options.BLOCKCHAIN_HASH_DIFFICULTY=0]
 *  Blockchain hash generation difficulty
 * @param {boolean} [options.WITH_BLOCKCHAIN_HASH=false]
 *  Does the model have a blockchain hash generated version
 *  after version
 * @param {boolean} [options.BLOCKCHAIN_HASH_GENESIS=0000000000000000000000000000000000000000000000000000000000000000]
 *  Genesis hash value
 *
 *  The snapshots collection name
 * @return {EventSourced} Configured EventSourced class
 */
export function EventSourcedFactory(
  collectionName,
  reducer,
  options: Partial<Options> = {},
) {
  return class EventSourced {
    static get options(): Options {
      return Object.assign(
        {
          CORRELATION_FIELD: 'correlation_id',
          CURRENT_HASH_FIELD: 'hash',
          PREVIOUS_HASH_FIELD: 'prev',
          NONCE_FIELD: 'nonce',
          CREATED_AT_FIELD: 'created_at',
          EVENTS_COLLECTION_NAME: `${collectionName}_events`,
          SNAPSHOTS_COLLECTION_NAME: `${collectionName}_snapshots`,
          WITH_GLOBAL_VERSION: false,
          WITH_BLOCKCHAIN_HASH: false,
          BLOCKCHAIN_HASH_DIFFICULTY: 0,
          BLOCKCHAIN_HASH_GENESIS:
            '0000000000000000000000000000000000000000000000000000000000000000',
        },
        Object.fromEntries(
          Object.entries(options).filter(([, v]) => v !== undefined),
        ),
      );
    }

    static get ERRORS() {
      return ERRORS;
    }

    /**
     * @param {object} db MongoDb connection
     * @param {string} collName Collection name to retrieve
     * @returns {object} MongoDb collection
     */
    static getCollection(db, collName) {
      return db.collection(collName);
    }

    /**
     * @param {object} db MongoDb connection
     * @returns {object} MongoDb collection
     */
    static getStatesCollection(db) {
      return EventSourced.getCollection(db, collectionName);
    }

    /**
     * @param {object} db MongoDb connection
     * @returns {object} MongoDb collection
     */
    static getEventsCollection(db) {
      return EventSourced.getCollection(
        db,
        EventSourced.options.EVENTS_COLLECTION_NAME,
      );
    }

    /**
     * @param {object} db MongoDb connection
     * @returns {object} MongoDb collection
     */
    static getSnapshotsCollection(db) {
      return EventSourced.getCollection(
        db,
        EventSourced.options.SNAPSHOTS_COLLECTION_NAME,
      );
    }

    /** Initialize the indexes
     * Mainly for testing purposes
     *
     * @param {object} db MongoDb connection
     * @returns {Promise} The promise of index creation
     */
    static initIndexes(db) {
      return Promise.all([
        ...EventSourced.getRequiredIndexes()[collectionName].map(
          ([fields, opts]) =>
            EventSourced.getStatesCollection(db).createIndex(fields, opts),
        ),
        Events.initIndexes(
          EventSourced.getEventsCollection(db),
          EventSourced.options.CORRELATION_FIELD,
          EventSourced.options.WITH_GLOBAL_VERSION,
        ),
        Snapshots.initIndexes(
          EventSourced.getSnapshotsCollection(db),
          EventSourced.options.CORRELATION_FIELD,
        ),
      ]);
    }

    /** Get the required indexes
     *
     * @returns {Array} Array of index spec [fields, options]
     */
    static getRequiredIndexes(): {
      [x: string]: (
        | object
        | {
            version: number;
            name: string;
            unique?: boolean;
          }
      )[][];
    } {
      return {
        [collectionName]: [
          [
            { [EventSourced.options.CORRELATION_FIELD]: 1 },
            { unique: true, name: 'correlation_id_unicity' },
          ],
          [
            { [EventSourced.options.CREATED_AT_FIELD]: 1 },
            { name: 'created_at_1' },
          ],
          [{ version: 1 }, { name: 'version_1' }],
        ],
        [EventSourced.options.EVENTS_COLLECTION_NAME]:
          Events.getRequiredIndexes(
            EventSourced.options.CORRELATION_FIELD,
            EventSourced.options.WITH_GLOBAL_VERSION,
          ),
        [EventSourced.options.SNAPSHOTS_COLLECTION_NAME]:
          Snapshots.getRequiredIndexes(EventSourced.options.CORRELATION_FIELD),
      };
    }

    /**
     * Returns the version of the state given in parameter
     * @param {object} state State
     * @returns {number} the version of the state: returns -1 if version is undefined
     */
    static getVersion(state) {
      if (state && typeof state.version !== 'undefined') {
        return state.version;
      }

      return -1;
    }

    /**
     * Returns events from the snapshot.version, state version or 0
     * @param {object} db MongoDb connection
     * @param {string} correlationId The correlation id
     * @param {number} version The version
     * @returns {object} MongoDb Cursor
     */
    static getEvents(db, correlationId, version) {
      return Events.getEvents(
        EventSourced.getEventsCollection(db),
        EventSourced.options.CORRELATION_FIELD,
        correlationId,
        version,
      );
    }

    /**
     * Returns the last index available in the events
     * collection
     * @param {object} db MongoDb connection
     * @returns {Promise<any>} Last available event
     */
    static getLastEvent(db) {
      return Events.getLastEvent(EventSourced.getStatesCollection(db));
    }

    /**
     * Returns the last events available for a given correlation ID
     * @param {object} db MongoDb connection
     * @param {string} correlationField Field used for correlation
     * @param {string} correlationId Entity correlation ID
     * @param {number} quantity Number of events to return
     * @returns {Promise<any>} Last available event
     */
    static getLastEvents(db, correlationField, correlationId, quantity) {
      return Events.getLastEvents(
        EventSourced.getEventsCollection(db),
        correlationField,
        correlationId,
        quantity,
      );
    }

    /**
     * Compute the hash for the blockchain mode
     * @param {number} version
     * @param {Date} createdAt
     * @param {object} state
     * @param {string} [precedingHash=' ']
     * @param {number} [nonce=0]
     * @returns {string}
     */
    static computeHash(
      version: number,
      createdAt: Date,
      state: any,
      nonce: number,
      precedingHash: string,
    ): string {
      const hash = crypto.createHash('sha256');

      hash.update(
        `${version}${precedingHash}${createdAt.getTime()}${JSON.stringify(
          omit(state, [
            EventSourced.options.CURRENT_HASH_FIELD,
            EventSourced.options.PREVIOUS_HASH_FIELD,
            EventSourced.options.NONCE_FIELD,
          ]),
        )}${nonce}`,
      );

      return hash.digest('hex');
    }

    /**
     * Computes a difficult blockchain hash
     * @param {number} version
     * @param {Date} createdAt
     * @param {object} state
     * @param {string} [precedingHash]
     * @returns {string}
     */
    static computeDifficultHash(
      version: number,
      createdAt: Date,
      state: any,
      precedingHash: string,
    ): { hash: string; nonce: number } {
      const difficulty: number =
        EventSourced.options.BLOCKCHAIN_HASH_DIFFICULTY;
      let nonce: number = 0;
      let hash = EventSourced.computeHash(
        version,
        createdAt,
        state,
        nonce,
        precedingHash,
      );

      while (hash.startsWith(Array(difficulty + 1).join('0')) !== true) {
        nonce += 1;
        hash = EventSourced.computeHash(
          version,
          createdAt,
          state,
          nonce,
          precedingHash,
        );
      }

      return { hash, nonce };
    }

    static getVersionFrom(eventVersion, stateVersion, globalVersion): number {
      if (eventVersion !== -1) {
        return eventVersion;
      }

      if (
        options.WITH_GLOBAL_VERSION === true &&
        globalVersion > stateVersion
      ) {
        return globalVersion + 1;
      }

      return stateVersion + 1;
    }

    static async reduceStep(correlationId, updated, event, lastEvent) {
      const eventVersion = EventSourced.getVersion(event);
      const stateVersion = EventSourced.getVersion(updated);
      const globalVersion = EventSourced.getVersion(lastEvent);

      if (
        options.WITH_GLOBAL_VERSION !== true &&
        eventVersion !== -1 &&
        eventVersion - stateVersion !== 1
      ) {
        throw EventSourced.ERRORS.INCOMPATIBLE_EVENT_VERSION_WITH_STATE_VERSION;
      }

      const precedingHash =
        (options.WITH_GLOBAL_VERSION === true && globalVersion > stateVersion
          ? lastEvent?.[EventSourced.options.CURRENT_HASH_FIELD]
          : updated?.[EventSourced.options.CURRENT_HASH_FIELD]) ||
        EventSourced.options.BLOCKCHAIN_HASH_GENESIS;

      // Apply a timestamp to the event:
      event[EventSourced.options.CREATED_AT_FIELD] = new Date(
        event[EventSourced.options.CREATED_AT_FIELD] || date.getNow().getTime(),
      );

      updated = await reducer(updated, event);

      // Force the event version tagging
      updated.version = EventSourced.getVersionFrom(
        eventVersion,
        stateVersion,
        globalVersion,
      );

      // Apply the version to the event
      event.version = EventSourced.getVersion(updated);

      // Attach the correlation field and id to the event and the state
      updated[EventSourced.options.CORRELATION_FIELD] = correlationId;
      event[EventSourced.options.CORRELATION_FIELD] = correlationId;

      // Blockchain logic
      if (options.WITH_BLOCKCHAIN_HASH === true) {
        const { hash, nonce } = EventSourced.computeDifficultHash(
          event.version,
          event.created_at,
          event,
          precedingHash,
        );
        updated[EventSourced.options.CURRENT_HASH_FIELD] = hash;
        updated[EventSourced.options.NONCE_FIELD] = nonce;

        event[EventSourced.options.PREVIOUS_HASH_FIELD] = precedingHash;
        event[EventSourced.options.CURRENT_HASH_FIELD] =
          updated[EventSourced.options.CURRENT_HASH_FIELD];
        event[EventSourced.options.NONCE_FIELD] =
          updated[EventSourced.options.NONCE_FIELD];
      }

      return deepFreeze(updated);
    }

    /**
     * Reduce events on the given state
     * @param {object[]} events Array of events
     * @param {object} state Versioned state
     * @param {string} correlationId CorrelationId
     * @param {number} [lastEvent] Last event available
     * @returns {[object, object[]]} The state and the updated events
     */
    static async reduce(events, state, correlationId, lastEvent?) {
      assert(
        correlationId,
        '[EventSourced#reduce] correlationId must be defined',
      );

      const copiedEvents = cloneDeep(events);
      let updatedState = deepFreeze(cloneDeep(state));
      for (const event of copiedEvents) {
        updatedState = await EventSourced.reduceStep(
          correlationId,
          updatedState,
          event,
          lastEvent,
        );
      }

      return [updatedState, copiedEvents];
    }

    /**
     * Returns an object containing the last updated state available in database and
     * the restored state from the database (last updated state + events missing reduced)
     *
     * @param {object} db MongoDb connection
     * @param {string} correlationId Correlation ID
     * @param {integer} version Target version
     *
     * @returns {object} An object containing the last updated state in database and restored state
     */
    static async getStateAtVersion(
      db,
      correlationId,
      version,
      mustThrow = true,
    ) {
      const snapshot = await Snapshots.getSnapshot(
        EventSourced.getSnapshotsCollection(db),
        EventSourced.options.CORRELATION_FIELD,
        correlationId,
        version,
      );

      if (snapshot !== null && snapshot.version === version) {
        return deepFreeze(snapshot);
      }

      const cursor = await EventSourced.getEvents(
        db,
        correlationId,
        EventSourced.getVersion(snapshot),
      );

      let currentState = cloneDeep(snapshot);

      while (await cursor.hasNext()) {
        // eslint-disable-line no-await-in-loop
        const event = await cursor.next();

        if (event.version > version) {
          break;
        }

        const [updated] = await EventSourced.reduce(
          [event],
          currentState,
          correlationId,
        );
        currentState = updated;

        if (currentState.version === version) {
          break;
        }
      }

      if (
        mustThrow === true &&
        options.WITH_GLOBAL_VERSION !== true &&
        currentState.version !== version
      ) {
        throw new Error('State version does not exist');
      }

      return deepFreeze(currentState);
    }

    /**
     * Returns an object containing the last updated state available in database and
     * the restored state from the database (last updated state + events missing reduced)
     *
     * @param {object} db MongoDb connection
     * @param {string} correlationId Correlation ID
     *
     * @returns {object} An object containing the last updated state in database and restored state
     */
    static async getState(db, correlationId) {
      const [stateInDb, lastEvents] = await Promise.all([
        EventSourced.getStatesCollection(db).findOne(
          {
            [EventSourced.options.CORRELATION_FIELD]: correlationId,
          },
          {
            projection: { _id: 0 },
          },
        ),
        EventSourced.getLastEvents(
          db,
          EventSourced.options.CORRELATION_FIELD,
          correlationId,
          10,
        ),
      ]);

      let currentState = cloneDeep(stateInDb);

      const notReducedEvents = lastEvents
        .filter(
          (e) =>
            EventSourced.getVersion(e) > EventSourced.getVersion(stateInDb),
        )
        .reverse();
      const knowsAllEvents =
        lastEvents.length === 0 || notReducedEvents.length < lastEvents.length;

      if (knowsAllEvents === true) {
        const [updated] = await EventSourced.reduce(
          notReducedEvents,
          currentState,
          correlationId,
        );

        currentState = updated;

        return {
          stateInDb: deepFreeze(stateInDb),
          currentState: deepFreeze(currentState),
        };
      }

      const cursor = await EventSourced.getEvents(
        db,
        correlationId,
        EventSourced.getVersion(stateInDb),
      );

      while (await cursor.hasNext()) {
        // eslint-disable-line no-await-in-loop
        const event = await cursor.next();

        const [updated] = await EventSourced.reduce(
          [event],
          currentState,
          correlationId,
        );
        currentState = updated;
      }

      return {
        stateInDb: deepFreeze(stateInDb),
        currentState: deepFreeze(currentState),
      };
    }

    /**
     * Stores events into the events collection
     * @param {object} db MongoDb connection
     * @param {object[]} events Events to store
     * @returns {object} MongoDb insertMany operation
     */
    static storeEvents(db, events) {
      return Events.store(EventSourced.getEventsCollection(db), events);
    }

    /**
     * Store the current object state into the database
     * @param {object} db MongoDb connection
     * @param {object} state The instance state
     * @param {number} [previousVersion] The previous available version state
     * @returns {object} MongoDb findOneAndReplace operation
     */
    static storeState(db, state, previousVersion) {
      return EventSourced.getStatesCollection(db).findOneAndReplace(
        {
          [EventSourced.options.CORRELATION_FIELD]:
            state[EventSourced.options.CORRELATION_FIELD],
          version: {
            $gte: previousVersion,
            $lt: EventSourced.getVersion(state),
          },
        },
        state,
        {
          upsert: true,
        },
      );
    }

    /**
     * Create a snapshot from the current available state
     * @param {object} db MongoDb connection
     * @param {string} correlationId The state correlation id
     * @returns {Promise} The snapshot insert operation
     */
    static async createSnapshot(db, correlationId) {
      // Retrieve the last available state in collection
      const { stateInDb } = await EventSourced.getState(db, correlationId);

      await Snapshots.create(
        EventSourced.getSnapshotsCollection(db),
        EventSourced.options.CORRELATION_FIELD,
        stateInDb,
      );

      return stateInDb;
    }

    db: Db;
    /* istanbul ignore next */
    logger: Logger = {
      info: () => null,
      warn: () => null,
      error: () => null,
    };
    correlationId: string = null;
    latestHandledEvents: any[] = [];

    /**
     * EventSourced class constructor
     *
     * @param {object} services External services
     * @param {object} services.db MongoDb connection
     * @param {object} services.logger The logger instance
     * @param {string} correlationId The instance correlationId
     */
    constructor({ db, logger }: { db: Db; logger?: Logger }, correlationId) {
      this.db = db;
      this.logger = logger || this.logger;
      this.correlationId = correlationId;

      this.defaultStoreStateErrorHandler =
        this.defaultStoreStateErrorHandler.bind(this);
    }

    /**
     * default store state handler, does nothing by default because state storage is not critical
     * @param {object} err error thrown when attempted to store the state
     * @param {object} updatedState error thrown when attempted to store the state
     * @return {void} nothing
     */
    defaultStoreStateErrorHandler(err, updatedState) {
      this.logger.info(
        { err, updatedState },
        'Error occured when attempted to save the state',
      );
    }

    /**
     * Execute a command handler returning events
     * @param {function} handler The command handler
     * @param {function} [storeStateErrorHandler] handle state storage failures, used to be for
     * loggin purpose
     * @return {object} updated state
     */
    async handle(
      handler: Function,
      storeStateErrorHandler: Function = this.defaultStoreStateErrorHandler,
      handleOptions: {
        imperativeVersion?: number;
        isReadonly?: string;
        mustWaitStatePersistence?: boolean;
      } = {},
    ) {
      assert(
        typeof storeStateErrorHandler === 'function',
        'storeStateErrorHandler must be a function',
      );

      // Retrieve last available state and current state with potential missing events reduced
      const { stateInDb, currentState } = await EventSourced.getState(
        this.db,
        this.correlationId,
      );

      if (
        handleOptions.isReadonly !== undefined &&
        currentState !== null &&
        currentState[handleOptions.isReadonly] === true
      ) {
        throw EventSourced.ERRORS.ENTITY_IS_READONLY;
      }

      // Apply the business logic and retrieve corresponding events
      const events = await handler(currentState);

      let lastEvent: any;
      if (options.WITH_GLOBAL_VERSION === true) {
        lastEvent = await EventSourced.getLastEvent(this.db);
      }

      // Reduce the events to the state
      const [updatedState, versionedEvents] = await EventSourced.reduce(
        events,
        currentState,
        this.correlationId,
        lastEvent,
      );

      if (
        handleOptions.imperativeVersion !== undefined &&
        updatedState.version !== handleOptions.imperativeVersion
      ) {
        throw EventSourced.ERRORS.HANDLER_IMPERATIVE_CONDITION_FAILED;
      }

      // Store the events
      await EventSourced.storeEvents(this.db, versionedEvents);
      this.latestHandledEvents.push(...versionedEvents);

      const promise = EventSourced.storeState(
        this.db,
        updatedState,
        EventSourced.getVersion(stateInDb),
      )
        .then(() => {
          return updatedState;
        })
        .catch((err) => {
          // Not important error
          storeStateErrorHandler(err, updatedState);

          return updatedState;
        });

      if (handleOptions.mustWaitStatePersistence !== false) {
        return promise;
      }

      /* istanbul ignore next */
      promise.catch((err) => {
        console.error(err);
      });

      return updatedState;
    }

    /**
     * Execute a command handler returning events
     * @param {function} handler The command handler
     * @param {number} retryDuration How long do we retry (milliseconds)
     * @param {function} [storeStateErrorHandler] handle state storage failures, used to be for
     * login purpose since state storage is not critical
     * @return {object} updated state
     */
    async handleWithRetry(
      handler,
      retryDuration,
      storeStateErrorHandler,
      handleOptions?,
    ) {
      assert(
        typeof retryDuration === 'number',
        'retryDuration must be a number',
      );

      const tic = Date.now();

      // eslint-disable-next-line no-await-in-loop
      while (Date.now() - tic < retryDuration) {
        try {
          return await this.handle(
            handler,
            storeStateErrorHandler,
            handleOptions,
          );
        } catch (err) {
          this.logger.error({ err }, 'Error while handling the command');
        }
      }

      throw EventSourced.ERRORS.HANDLER_RETRY_TIMEOUT;
    }

    /**
     * Create a snapshot from the current available state
     * @returns {Promise} The snapshot insert operation
     */
    createSnapshot() {
      return EventSourced.createSnapshot(this.db, this.correlationId);
    }
  };
}
