import { strict as assert } from 'assert';

export class Snapshots {
  /** Initialize the indexes
   * Mainly for testing purposes
   *
   * @param {object} collection MongoDb collection
   * @param {string} correlationField Field used for correlation
   * @returns {Promise} The promise of index creation
   */
  static initIndexes(collection, correlationField) {
    return Promise.all(
      Snapshots.getRequiredIndexes(correlationField).map(([fields, options]) =>
        collection.createIndex(fields, options),
      ),
    );
  }

  /** Get the required indexes
   *
   * @param {string} correlationField Field used for correlation
   * @returns {Array} Array of index spec [fields, options]
   */
  static getRequiredIndexes(correlationField) {
    return [
      [
        {
          [correlationField]: 1,
          version: 1,
        },
        {
          unique: true,
          name: 'correlation_id_version_unicity',
        },
      ],
    ];
  }

  /**
   * Returns the snapshots corresponding to a specific version or prior to it
   * @param {object} collection MongoDb collection
   * @param {string} correlationField Field used for correlation
   * @param {string} correlationId The events correlation id
   * @param {number} version The first event version
   * @returns {object} MongoDb Cursor
   */
  static getSnapshot(collection, correlationField, correlationId, version) {
    assert(
      correlationField,
      '[Snapshots#getSnapshot] Missing correlationField',
    );
    assert(correlationId, '[Snapshots#getSnapshot] Missing correlationId');
    assert(
      typeof version === 'number',
      '[Snapshots#getSnapshot] Invalid version (must be a number)',
    );

    return collection.findOne(
      {
        [correlationField]: correlationId,
        version: {
          $lte: version,
        },
      },
      {
        sort: { version: -1 },
        projection: { _id: 0 },
      },
    );
  }

  /**
   * Create a state snapshot from a state
   * @param {object} collection MongoDb collection
   * @param {string} correlationField Field used for correlation
   * @param {object} state The state to backup
   * @returns {object} MongoDb result
   */
  static create(collection, correlationField, state) {
    assert(state !== null, '[Snapshots#create] State can not be null');

    return collection.findOneAndUpdate(
      {
        [correlationField]: state[correlationField],
        version: state.version,
      },
      { $setOnInsert: state },
      {
        projection: { _id: 0 },
        upsert: true,
      },
    );
  }
}
