import { strict as assert } from 'assert';

/**
 * Events class to manage events for EventSourced instances
 *
 * Questions:
 * - how do you control that events have consecutive versions?
 * - how do you automate the event versioning?
 */
export class Events {
  /** Initialize the indexes
   * Mainly for testing purposes
   *
   * @param {object} collection MongoDb collection
   * @param {string} correlationField Field used for correlation
   * @param {boolean} [withGlobalVersion] Do we use a global index
   * @returns {Promise} The promise of index creation
   */
  static initIndexes(collection, correlationField, withGlobalVersion) {
    return Promise.all(
      Events.getRequiredIndexes(correlationField, withGlobalVersion).map(
        ([fields, options]) => collection.createIndex(fields, options),
      ),
    );
  }

  /** Get the required indexes
   *
   * @param {string} correlationField Field used for correlation
   * @param {boolean} [withGlobalVersion] Do we use a global index
   * @returns {Array} Array of index spec [fields, options]
   */
  static getRequiredIndexes(correlationField, withGlobalVersion) {
    const indexes: [any, any][] = [
      [
        {
          created_at: 1,
        },
        {
          name: 'created_at_1',
        },
      ],
      [
        {
          version: 1,
          created_at: 1,
        },
        {
          name: 'version_1_created_at_1',
        },
      ],
    ];

    if (withGlobalVersion === true) {
      indexes.push([
        {
          version: 1,
        },
        {
          unique: true,
          name: 'version_unicity',
        },
      ]);
    } else {
      indexes.push(
        [
          {
            version: 1,
          },
          {
            name: 'version_1',
          },
        ],
        [
          {
            [correlationField]: 1,
            version: 1,
          },
          {
            unique: true,
            name: 'correlation_id_version_unicity',
          },
        ],
      );
    }

    return indexes;
  }

  /**
   * Returns events for a given correlation id
   * @param {object} collection MongoDb collection
   * @param {string} correlationField Field used for correlation
   * @param {string} correlationId The events correlation id
   * @param {number} version The first event version
   * @returns {object} MongoDb Cursor
   */
  static getEvents(collection, correlationField, correlationId, version) {
    assert(correlationField, '[Events#getEvents] Missing correlationField');
    assert(correlationId, '[Events#getEvents] Missing correlationId');
    assert(
      typeof version === 'number',
      '[Events#getEvents] Invalid version (must be a number)',
    );

    return collection
      .find(
        {
          [correlationField]: correlationId,
          version: {
            $gt: version,
          },
        },
        {
          projection: {
            _id: 0,
            [correlationField]: 0,
            // created_at: 0,
          },
        },
      )
      .sort({ version: 1 });
  }

  /**
   * Returns the last index available in the events
   * collection
   * @param {object} collection MongoDb collection
   * @returns {Promise<any>} Last available event
   */
  static async getLastEvent(collection): Promise<any> {
    return collection.findOne(
      {},
      {
        projection: {
          _id: 0,
        },
        sort: {
          version: -1,
        },
      },
    );
  }

  /**
   * Returns the last events available for a given correlation ID
   * @param {object} collection MongoDb collection
   * @param {string} correlationField Field used for correlation
   * @param {string} correlationId Entity correlation ID
   * @param {number} quantity Number of events to return
   * @returns {Promise<any>} Last available event
   */
  static async getLastEvents(
    collection,
    correlationField,
    correlationId,
    quantity = 10,
  ): Promise<any> {
    assert(correlationField, '[Events#getLastEvents] Missing correlationField');
    assert(correlationId, '[Events#getLastEvents] Missing correlationId');
    assert(
      typeof quantity === 'number',
      '[Events#getLastEvents] Invalid version (must be a number)',
    );

    return collection
      .find(
        {
          [correlationField]: correlationId,
        },
        {
          projection: {
            _id: 0,
            [correlationField]: 0,
          },
        },
      )
      .sort({ version: -1 })
      .limit(quantity)
      .toArray();
  }

  /**
   * Store an event into the database
   * @param {object} collection MongoDb collection
   * @param {object[]} events The event to store
   * @returns {object} MongoDb result
   */
  static store(collection, events) {
    return collection.insertMany(events);
  }
}
