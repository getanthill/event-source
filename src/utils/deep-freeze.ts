/**
 * Deep freeze an object
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze
 * @param {object} obj The object to freeze
 * @returns {object} The freezed object
 */
export function deepFreeze(obj) {
  // Retrieve the property names defined on obj
  const propNames = Object.getOwnPropertyNames(obj || {});

  // Freeze properties before freezing self
  propNames.forEach((name) => {
    const prop = obj[name];

    if (prop instanceof Buffer) {
      return;
    }

    // Freeze prop if it is an object
    if (typeof prop === 'object' && prop !== null) {
      deepFreeze(prop);
    }
  });

  // Freeze self (no-op if already frozen)
  return Object.freeze(obj);
}
