/**
 * Return actual date, creating to be able to stub the date in the tests
 * @return {Date} actual date
 */
export function getNow() {
  return new Date();
}
