import type { ObjectId } from '@getanthill/mongodb-connector';

export interface BasicIndexes {
  [key: string]: object[][];
}

export interface BasicState {
  version: number;
  _id: ObjectId;
}

export interface BasicEventState extends BasicState {
  type: string;
  v: number;
  created_at: Date;
}

export interface BasicEvent {
  type: string;
  v: number;
  [key: string]: any;
}

export interface EventSourcedOptionInitialization {
  CORRELATION_FIELD?: string;
  EVENTS_COLLECTION_NAME?: string;
  SNAPSHOTS_COLLECTION_NAME?: string;
}

export interface EventSourcedOption {
  CORRELATION_FIELD: string;
  EVENTS_COLLECTION_NAME: string;
  SNAPSHOTS_COLLECTION_NAME: string;
}

export interface ERROR {
  INCOMPATIBLE_EVENT_VERSION_WITH_STATE_VERSION: Error;
  HANDLER_RETRY_TIMEOUT: Error;
}
