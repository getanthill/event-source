import { Collection, FindCursor } from 'mongodb';
import {BasicEvent, BasicEventState} from "./interfaces";

type indexes = object[][];

export class Events {
  static initIndexes(collection: Collection, correlationField): Promise<indexes>;
  static getRequiredIndexes(correlationField: string): indexes;
  static getEvents(collection: Collection, correlationField: string, correlationId: any, version: number): FindCursor;
  static store(collection: Collection, event: BasicEvent): BasicEventState;
}
