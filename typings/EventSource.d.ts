import { Db, Collection, FindCursor } from 'mongodb';
import {
  BasicEvent,
  BasicIndexes,
  BasicState,
  ERROR,
  EventSourcedOption,
  EventSourcedOptionInitialization,
} from './interfaces';

export default function eventSourcedFactory<
  T extends BasicState,
  F extends BasicEvent
>(
  collectionName: string,
  reducer: ReduceFn<T, F>,
  options: EventSourcedOptionInitialization,
): typeof EventSourced;

type ReduceFn<T extends BasicState, F extends BasicEvent> = (
  state: T | null,
  event: F,
) => { [key: string]: any };
type HandlerFN<T extends BasicState> = (state: T) => BasicEvent[];
type storeError<T extends BasicState> = (err: Error, updateState: T) => void;

declare class EventSourced<T extends BasicState> {
  public static options(): EventSourcedOption;

  public static ERRORS(): ERROR;

  public static getCollection(db: Db, collName: string): Collection;
  public static getStatesCollection(db: Db): Collection;
  public static getEventsCollection(db: Db): Collection;
  public static getSnapshotsCollection(db: Db): Collection;

  public static getRequiredIndexes(): BasicIndexes;

  public static initIndexes(db: Db): Promise<BasicIndexes>;

  public static getEvents<T extends BasicState>(
    db: Db,
    correlationId: any,
  ): FindCursor<T>;
  public static getVersion<T extends BasicState>(state: T): number;

  public static getState<T extends BasicState>(db: Db, correlationId: any): T;

  public handleWithRetry(
    handler: HandlerFN<T>,
    retryDuration: number,
    storeStateErrorHandler?: storeError<T>,
  ): Promise<BasicState>;

  public handle(
    handler: HandlerFN<T>,
    storeStateErrorHandler?: storeError<T>,
  ): Promise<T>;

  public createSnapshot(): T;

  constructor(db: Db, correlationId: any);
}
