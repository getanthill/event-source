# [0.16.0](https://gitlab.com/getanthill/event-source/compare/v0.15.0...v0.16.0) (2024-08-01)


### Features

* **reducer:** allows to define async reducer ([6e01b13](https://gitlab.com/getanthill/event-source/commit/6e01b13234d11fbc436162226484e2d7e46f7c43))

# [0.15.0](https://gitlab.com/getanthill/event-source/compare/v0.14.1...v0.15.0) (2023-12-27)


### Bug Fixes

* create default index on version and created_at fields ([994303f](https://gitlab.com/getanthill/event-source/commit/994303f4a6c912d9a151b431acd5a5cd642c7cd7))
* **sonar:** use "startsWith" instead of "substring" ([c74a812](https://gitlab.com/getanthill/event-source/commit/c74a81222bcc17a3e68e3f1fa8142d2c41c3f4b2))


### Features

* bump to Node 20 ([92fba39](https://gitlab.com/getanthill/event-source/commit/92fba39a7b6218255d129e8e314ec0dfdf4baf4f))

## [0.14.1](https://gitlab.com/getanthill/event-source/compare/v0.14.0...v0.14.1) (2023-05-02)


### Bug Fixes

* adds default indexes to entities ([47e9c10](https://gitlab.com/getanthill/event-source/commit/47e9c10ec4e23520c00ea36e748437f974f38991))

# [0.14.0](https://gitlab.com/getanthill/event-source/compare/v0.13.1...v0.14.0) (2023-05-02)


### Bug Fixes

* **dep:** bump dependencies ([cd9698f](https://gitlab.com/getanthill/event-source/commit/cd9698fc1ba17ec1983e36cc1d0d40c7f1c09fc8))


### Features

* adds default indexes to events collections ([c92a42b](https://gitlab.com/getanthill/event-source/commit/c92a42b27207b3aba041b0cb6c652f1b763d713c))

## [0.12.6](https://gitlab.com/getanthill/event-source/compare/v0.12.5...v0.12.6) (2023-01-16)


### Bug Fixes

* catch promise error in case of deferred state persistence ([a9d1fe8](https://gitlab.com/getanthill/event-source/commit/a9d1fe826e8dbb74cef34b9bca5a0b548f2de88b))

## [0.12.5](https://gitlab.com/getanthill/event-source/compare/v0.12.4...v0.12.5) (2023-01-16)


### Bug Fixes

* remove correlation field from returned last events function ([669ed12](https://gitlab.com/getanthill/event-source/commit/669ed12a23692604eff4d404d7adb56f03ee3f7f))

## [0.12.4](https://gitlab.com/getanthill/event-source/compare/v0.12.3...v0.12.4) (2023-01-16)


### Bug Fixes

* **perf:** improve performance requesting states and events in parallel for mot cases ([5c342b4](https://gitlab.com/getanthill/event-source/commit/5c342b4a5f715d3ef1ecc197f6cb4496ceabaa5f))

## [0.12.3](https://gitlab.com/getanthill/event-source/compare/v0.12.2...v0.12.3) (2023-01-12)


### Bug Fixes

* remove trace from logger typing ([e289cfa](https://gitlab.com/getanthill/event-source/commit/e289cfaa5f0d99990de1d651cfbcd7bfca9658ef))

## [0.12.2](https://gitlab.com/getanthill/event-source/compare/v0.12.1...v0.12.2) (2023-01-12)


### Bug Fixes

* update dependencies ([e63ec6c](https://gitlab.com/getanthill/event-source/commit/e63ec6c20fcac50ffae02ba02bf49eb2b362cab7))

## [0.12.1](https://gitlab.com/getanthill/event-source/compare/v0.12.0...v0.12.1) (2022-11-24)


### Bug Fixes

* **ts:** fix function output typing ([2162ed3](https://gitlab.com/getanthill/event-source/commit/2162ed344445b5ffb2a4d414d94696c32f67d6e2))

# [0.12.0](https://gitlab.com/getanthill/event-source/compare/v0.11.0...v0.12.0) (2022-11-22)


### Features

* upgrade Node.js to 18 ([06f7e44](https://gitlab.com/getanthill/event-source/commit/06f7e44fa703650d417decda9d1e8342918e1475))

# [0.11.0](https://gitlab.com/getanthill/event-source/compare/v0.10.2...v0.11.0) (2022-11-18)


### Features

* stores latest handled events in the EventSourced instance ([3196ab6](https://gitlab.com/getanthill/event-source/commit/3196ab60445c9c21cb48e5e2bfc272d7c96ef652))

## [0.10.2](https://gitlab.com/getanthill/event-source/compare/v0.10.1...v0.10.2) (2022-11-14)


### Bug Fixes

* update dependencies ([26b5d06](https://gitlab.com/getanthill/event-source/commit/26b5d06e0cdd1d00226e3bff67c03e7443d57f6f))

## [0.10.1](https://gitlab.com/getanthill/event-source/compare/v0.10.0...v0.10.1) (2022-11-14)


### Bug Fixes

* update package.json to inform node version >=16 supported ([1667a62](https://gitlab.com/getanthill/event-source/commit/1667a6267d0c030c62f30f235fde21658f063108))

# [0.10.0](https://gitlab.com/getanthill/event-source/compare/v0.9.4...v0.10.0) (2022-06-21)


### Features

* make the state persistence waiting optional for handler ([1c4fbc7](https://gitlab.com/getanthill/event-source/commit/1c4fbc7c4d33d5005ec8943819efedfab1feeb5d))

## [0.9.3](https://gitlab.com/getanthill/event-source/compare/v0.9.2...v0.9.3) (2022-05-20)


### Bug Fixes

* **global:** supports timetravel with global version index ([fb4d9a2](https://gitlab.com/getanthill/event-source/commit/fb4d9a23ff957c65399763e0c712af3df33d29b3))

## [0.9.2](https://gitlab.com/getanthill/event-source/compare/v0.9.1...v0.9.2) (2022-05-19)


### Bug Fixes

* **config:** filter config properties not defined ([115a767](https://gitlab.com/getanthill/event-source/commit/115a7678ec7e542accc1080024db59f77c41f148))

## [0.9.1](https://gitlab.com/getanthill/event-source/compare/v0.9.0...v0.9.1) (2022-05-19)


### Bug Fixes

* ensure using `epoch` for timestamp ([d68f7a8](https://gitlab.com/getanthill/event-source/commit/d68f7a8add4fe688181d17dabcc3d016392509d8))

# [0.9.0](https://gitlab.com/getanthill/event-source/compare/v0.8.0...v0.9.0) (2022-05-19)


### Features

* supports blockchain event-sourcing ([65ae6d0](https://gitlab.com/getanthill/event-source/commit/65ae6d0636a77a5d1e5bbda85526dd625dce2e55))

# [0.8.0](https://gitlab.com/getanthill/event-source/compare/v0.7.1...v0.8.0) (2022-05-19)


### Features

* allows to have events `version` globally defined ([12bca04](https://gitlab.com/getanthill/event-source/commit/12bca04fbb8dd17a96095fde8b551fc4c8e733e0))

## [0.7.1](https://gitlab.com/getanthill/event-source/compare/v0.7.0...v0.7.1) (2022-05-04)


### Bug Fixes

* rollback correlation field addition ([2399186](https://gitlab.com/getanthill/event-source/commit/239918669d8918e515aea033d33443d17711513c))


### Reverts

* Revert "feat: returns the correlation field while getting events" ([0f5df25](https://gitlab.com/getanthill/event-source/commit/0f5df25399143e67ba81cb7dd4a5082ff182e5d5))

# [0.7.0](https://gitlab.com/getanthill/event-source/compare/v0.6.2...v0.7.0) (2022-05-04)


### Features

* returns the correlation field while getting events ([b57b6ff](https://gitlab.com/getanthill/event-source/commit/b57b6ff5182b5a9639b6981b5c76099bff70d49e))

## [0.5.2](https://gitlab.com/getanthill/event-source/compare/v0.5.1...v0.5.2) (2022-04-29)


### Bug Fixes

* bump version ([b60524d](https://gitlab.com/getanthill/event-source/commit/b60524df3471c2cf5eb36e1d2ee8123e411f48ef))
