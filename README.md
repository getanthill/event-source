# `getanthill` Event-Source

[![pipeline](https://gitlab.com/getanthill/event-source/badges/master/pipeline.svg?ignore_skipped=true)](https://gitlab.com/getanthill/event-source/-/commits/master) [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=getanthill%2Fevent-source&metric=alert_status)](https://sonarcloud.io/dashboard?id=getanthill%2Fevent-source)

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=getanthill%2Fevent-source&metric=coverage)](https://sonarcloud.io/dashboard?id=getanthill%2Fevent-source) [![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=getanthill%2Fevent-source&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=getanthill%2Fevent-source)

## Getting Started

### Install the package

```shell
npm install -S @getanthill/event-source
```

### Define your class

The following will be extended with the [Event-Source](https://martinfowler.com/eaaDev/EventSourcing.html)
functionalities.

```ts
// src/models/Users.ts
import { EventSourcedFactory } from '@getanthill/event-source';

/**
 * This is the main reducer for your events.
 * > f(state_{i}, event) = state_{i+1}
 */
const reducer = (state, event) => {
  if (event.type === 'CREATED') {
    return {
      firstname: event.firstname,
    };
  }

  return state;
};

export default class Users extends EventSourced('users', reducer) {
  /**
   * Create a new user
   * @param {string} firstname The user firstname
   * @return {Promise<object>} Updated state
   */
  create(firstname) {
    return this.handle(() => [
      {
        type: 'CREATED',
        firstname,
      },
    ]);
  }
}
```

### Use your Event-Sourced class

```ts
// src/business/users.ts
import Users from '../models/Users';

/**
 * Create a user
 *
 * @param {object} db MongoDb connection
 * @param {string} firstname User firstname
 * @return {Promise<object>} State after success
 */
function createUser(db, firstname) {
  const user = new Users(db, 1); // 1 = correlation_id

  return user.create('John');
}
```

In your database, we will now have access to the events in the collection
`users_events`:

```mongo
db.users_events.find().pretty();
```

```json
{
  "type": "CREATED",
  "correlation_id": 1,
  "version": 0,
  "firstname": "John",
  "created_at": "2020-11-01T06:05:43.210Z"
}
```

and also the final resulting state of your object:

```mongo
db.users.find().pretty();
```

```json
{
  "correlation_id": 1,
  "version": 0,
  "firstname": "John"
}
```

## Conclusion

Now that you are able to have a fully Event-Source featured class, you can
read further on what is possible to create with this library and how to custom
a bit some features such as the `correlation_id` naming.
