import { AssertionError } from 'assert';

import setup from '../../setup';

import { Events } from '../../../src/Events';

describe('Events', () => {
  describe('static', () => {
    describe('getEvents', () => {
      let mongodb;
      let collection;

      beforeAll(async () => {
        mongodb = await setup.initDb();

        collection = mongodb.DB.collection('events');
      });

      beforeEach(async () => {
        await collection.deleteMany();
      });

      afterAll(async () => {
        await setup.teardownDb(mongodb);
      });

      it('returns a cursor on events', () => {
        const cursor = Events.getEvents(
          collection,
          'correlation_field',
          'correlation_id',
          0,
        );

        expect(cursor.constructor.name).toEqual('FindCursor');
      });

      it('returns the list of events not reduced yet', async () => {
        await collection.insertMany([
          {
            correlation_id: '1',
            version: 0,
            type: 'CREATED',
            field: 'a',
            // added by the lib
            created_at: new Date(2019, 0, 1),
          },
          {
            correlation_id: '1',
            version: 1,
            type: 'UPDATED',
            field: 'b',
            // added by the lib
            created_at: new Date(2019, 1, 1),
          },
        ]);

        const cursor = Events.getEvents(collection, 'correlation_id', '1', 0);

        const events = await cursor.toArray();

        expect(events).toEqual([
          {
            created_at: new Date(2019, 1, 1),
            version: 1,
            type: 'UPDATED',
            field: 'b',
          },
        ]);
      });

      it('returns the entire list of events if the object state does not have a version', async () => {
        await collection.insertMany([
          {
            correlation_id: '1',
            version: 0,
            type: 'CREATED',
            field: 'a',
            // added by the lib
            created_at: new Date(2019, 0, 1),
          },
          {
            correlation_id: '1',
            version: 1,
            type: 'UPDATED',
            field: 'b',
            // added by the lib
            created_at: new Date(2019, 1, 1),
          },
        ]);

        const cursor = Events.getEvents(collection, 'correlation_id', '1', -1);

        const events = await cursor.toArray();

        expect(events).toEqual([
          {
            created_at: new Date(2019, 0, 1),
            version: 0,
            type: 'CREATED',
            field: 'a',
          },
          {
            created_at: new Date(2019, 1, 1),
            version: 1,
            type: 'UPDATED',
            field: 'b',
          },
        ]);
      });

      it('throws an error if no correlation field is given', () => {
        let error;

        try {
          // @ts-ignore
          Events.getEvents(collection);
        } catch (err) {
          error = err;
        }

        expect(error).toBeInstanceOf(AssertionError);
        expect(error).toHaveProperty(
          'message',
          '[Events#getEvents] Missing correlationField',
        );
      });

      it('throws an error if no correlation id is given', () => {
        let error;

        try {
          // @ts-ignore
          Events.getEvents(collection, 'correlation_id');
        } catch (err) {
          error = err;
        }

        expect(error).toBeInstanceOf(AssertionError);
        expect(error).toHaveProperty(
          'message',
          '[Events#getEvents] Missing correlationId',
        );
      });

      it('throws an error if the version is not a number', () => {
        let error;

        try {
          // @ts-ignore
          Events.getEvents(collection, 'correlation_id', '1');
        } catch (err) {
          error = err;
        }

        expect(error).toBeInstanceOf(AssertionError);
        expect(error).toHaveProperty(
          'message',
          '[Events#getEvents] Invalid version (must be a number)',
        );
      });
    });

    describe('getLastEvent', () => {
      let mongodb;
      let collection;

      beforeAll(async () => {
        mongodb = await setup.initDb();

        collection = mongodb.DB.collection('events');
      });

      beforeEach(async () => {
        await collection.deleteMany();
      });

      afterAll(async () => {
        await setup.teardownDb(mongodb);
      });

      it('returns null if no event is found', async () => {
        const event = await Events.getLastEvent(collection);

        expect(event).toEqual(null);
      });

      it('returns the event if one event exists', async () => {
        await collection.insertMany([
          {
            correlation_id: '1',
            version: 0,
            type: 'CREATED',
            field: 'a',
            // added by the lib
            created_at: new Date(2019, 0, 1),
          },
        ]);

        const event = await Events.getLastEvent(collection);

        expect(event).toEqual({
          correlation_id: '1',
          version: 0,
          type: 'CREATED',
          field: 'a',
          // added by the lib
          created_at: new Date(2019, 0, 1),
        });
      });

      it('returns the last event version', async () => {
        await collection.insertMany([
          {
            correlation_id: '1',
            version: 0,
            type: 'CREATED',
            field: 'a',
            // added by the lib
            created_at: new Date(2019, 0, 1),
          },
          {
            correlation_id: '1',
            version: 1,
            type: 'UPDATED',
            field: 'b',
            // added by the lib
            created_at: new Date(2019, 1, 1),
          },
        ]);

        const event = await Events.getLastEvent(collection);

        expect(event).toEqual({
          correlation_id: '1',
          version: 1,
          type: 'UPDATED',
          field: 'b',
          // added by the lib
          created_at: new Date(2019, 1, 1),
        });
      });
    });

    describe('getLastEvents', () => {
      let mongodb;
      let collection;

      beforeAll(async () => {
        mongodb = await setup.initDb();

        collection = mongodb.DB.collection('events');
      });

      beforeEach(async () => {
        await collection.deleteMany();
      });

      afterAll(async () => {
        await setup.teardownDb(mongodb);
      });

      it('returns an empty array if no event is found', async () => {
        const events = await Events.getLastEvents(
          collection,
          'correlation_field',
          'correlation_id',
        );

        expect(events).toEqual([]);
      });

      it('returns last events inversely ordered by version', async () => {
        await collection.insertMany([
          {
            correlation_id: '1',
            version: 0,
            type: 'CREATED',
            field: 'a',
            // added by the lib
            created_at: new Date(2019, 0, 1),
          },
          {
            correlation_id: '1',
            version: 1,
            type: 'UPDATED',
            field: 'b',
            // added by the lib
            created_at: new Date(2019, 1, 1),
          },
        ]);

        const events = await Events.getLastEvents(
          collection,
          'correlation_id',
          '1',
        );

        expect(events).toEqual([
          {
            version: 1,
            type: 'UPDATED',
            field: 'b',
            // added by the lib
            created_at: new Date(2019, 1, 1),
          },
          {
            version: 0,
            type: 'CREATED',
            field: 'a',
            // added by the lib
            created_at: new Date(2019, 0, 1),
          },
        ]);
      });

      it('returns only quantity last events if requested', async () => {
        await collection.insertMany([
          {
            correlation_id: '1',
            version: 0,
            type: 'CREATED',
            field: 'a',
            // added by the lib
            created_at: new Date(2019, 0, 1),
          },
          {
            correlation_id: '1',
            version: 1,
            type: 'UPDATED',
            field: 'b',
            // added by the lib
            created_at: new Date(2019, 1, 1),
          },
        ]);

        const events = await Events.getLastEvents(
          collection,
          'correlation_id',
          '1',
          1,
        );

        expect(events).toEqual([
          {
            version: 1,
            type: 'UPDATED',
            field: 'b',
            // added by the lib
            created_at: new Date(2019, 1, 1),
          },
        ]);
      });
    });
  });
});
