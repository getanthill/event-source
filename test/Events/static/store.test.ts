import { Events } from '../../../src/Events';

import setup from '../../setup';

describe('Events', () => {
  describe('static', () => {
    describe('store', () => {
      let mongodb;
      let collection;
      let nowStub;

      beforeAll(async () => {
        mongodb = await setup.initDb();

        collection = mongodb.DB.collection('events');
      });

      beforeEach(async () => {
        await Promise.all([
          collection.deleteMany(),
          Events.initIndexes(collection, 'correlation_id'),
        ]);
      });

      afterAll(async () => {
        await setup.teardownDb(mongodb);
      });

      afterEach(() => {
        jest.resetAllMocks();
      });

      it('stores events into the collection successfully', async () => {
        const events = [
          {
            created_at: new Date('2018-08-08T00:00:00.000Z'),
            type: 'CREATE',
            correlation_id: '1',
            version: 0,
          },
        ];

        await Events.store(collection, events);

        // not using getEvents method since if filters fields that are introduced by Events.store
        const storedEvents = await collection
          .find({}, { projection: { _id: 0 } })
          .toArray();

        expect(storedEvents).toEqual([
          {
            type: 'CREATE',
            correlation_id: '1',
            version: 0,
            created_at: new Date('2018-08-08T00:00:00.000Z'),
          },
        ]);
      });

      it('throws an error if trying to insert 2 times an event with the same correlation id and version', async () => {
        await Events.store(collection, [
          {
            type: 'CREATE',
            correlation_id: '1',
            version: 0,
          },
        ]);

        let error;

        try {
          await Events.store(collection, [
            {
              type: 'CREATE',
              correlation_id: '1',
              version: 0,
            },
          ]);
        } catch (err) {
          error = err;
        }

        expect(error).toBeInstanceOf(Error);
        expect(error).toHaveProperty('message');
        expect(error.message).toContain('E11000');
      });
    });
  });
});
