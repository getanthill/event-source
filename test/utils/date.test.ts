import * as date from '../../src/utils/date';

describe('utils/date', () => {
  describe('#getNow should return a date instance', () => {
    it('should return an instance of date', () => {
      const now = date.getNow();
      expect(now).toBeInstanceOf(Date);
    });
  });
});
