import { deepFreeze } from '../../src/utils/deep-freeze';

describe('utils/deep-freeze', () => {
  it('freezes an object', () => {
    const obj = deepFreeze({
      a: 1,
    });

    let error;

    try {
      obj.a = 2;
    } catch (err) {
      error = err;
    }

    expect(error).toBeInstanceOf(Error);
    expect(error).toHaveProperty('message');
    expect(error.message).toContain('Cannot assign to read only property');
  });

  it('freezes an object with Buffer (skip it)', () => {
    const obj = deepFreeze({
      a: Buffer.from('string'),
    });

    let error;

    try {
      obj.a = 2;
    } catch (err) {
      error = err;
    }

    expect(error).toBeInstanceOf(Error);
    expect(error).toHaveProperty('message');
    expect(error.message).toContain('Cannot assign to read only property');
  });

  it('freezes a nested object', () => {
    const obj = deepFreeze({
      a: {
        b: 1,
      },
    });

    let error;

    try {
      obj.a.b = 2;
    } catch (err) {
      error = err;
    }

    expect(error).toBeInstanceOf(Error);
    expect(error).toHaveProperty('message');
    expect(error.message).toContain('Cannot assign to read only property');
  });

  it('freezes an array of objects', () => {
    const obj = deepFreeze([
      {
        a: {
          b: 1,
        },
      },
    ]);

    let error;

    try {
      obj[0] = {
        a: 1,
      };
    } catch (err) {
      error = err;
    }

    expect(error).toBeInstanceOf(Error);
    expect(error).toHaveProperty('message');
    expect(error.message).toContain('Cannot assign to read only property');
  });
});
