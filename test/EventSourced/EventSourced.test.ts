import { AssertionError } from 'assert';

const logger = require('chpr-logger');

import setup from '../setup';

import { EventSourcedFactory } from '../../src/EventSourced';

import * as date from '../../src/utils/date';

describe('EventSourced', () => {
  let roomId: number;
  const INITIAL_STATE = {
    name: 'Standard',
    quantity: 10,
  };
  let DateOrigin = Date;
  let constantDate;

  const reducer = (state, event) => {
    if (event.type === 'CREATED') {
      return {
        ...INITIAL_STATE,
        name: event.name || INITIAL_STATE.name,
        quantity: event.quantity || INITIAL_STATE.quantity,
      };
    }

    if (event.type === 'NAME_UPDATED') {
      return {
        ...state,
        name: event.name,
      };
    }

    if (event.type === 'ROOMS_BOOKED') {
      return {
        ...state,
        quantity: state.quantity - event.quantity,
      };
    }

    if (event.type === 'IS_READONLY') {
      return {
        ...state,
        is_readonly: event.is_readonly,
      };
    }

    return state;
  };

  function buildRoomsClass() {
    return class extends EventSourcedFactory('rooms', reducer, {
      CORRELATION_FIELD: 'room_id',
    }) {
      /**
       * Create a new room
       * @param {string} name The room name
       * @param {number} quantity Room quantity
       * @return {Promise<object>} Updated state
       */
      create(name, quantity = 5, handleOptions?) {
        return this.handle(
          () => [
            {
              type: 'CREATED',
              name,
              quantity,
            },
          ],
          () => null,
          handleOptions,
        );
      }

      book(
        quantity = 1,
        withRetry = false,
        retryDuration,
        storeStateErrorHandler,
        handleOptions?,
      ) {
        const handler = (state) => {
          if (state.quantity - quantity < 0) {
            throw new Error('No more room');
          }

          return [
            {
              type: 'ROOMS_BOOKED',
              quantity,
            },
          ];
        };

        if (withRetry === true) {
          return this.handleWithRetry(
            handler,
            retryDuration,
            storeStateErrorHandler,
            handleOptions,
          );
        }

        return this.handle(handler, storeStateErrorHandler, handleOptions);
      }
    };
  }

  let Rooms;
  let mongodb;
  let db;

  beforeAll(async () => {
    mongodb = await setup.initDb();
    db = mongodb.DB;
  });

  beforeEach(async () => {
    constantDate = new Date('2020-11-10T00:00:00.000Z');

    // @ts-ignore
    global.Date = class extends Date {
      constructor() {
        super();

        return constantDate;
      }
    };

    Rooms = buildRoomsClass();

    await Promise.all([
      Rooms.getStatesCollection(db).deleteMany({}),
      Rooms.getEventsCollection(db).deleteMany({}),
      Rooms.getSnapshotsCollection(db).deleteMany({}),
    ]);

    await Rooms.initIndexes(db);

    roomId = Math.floor(setup.random() * 1e9);
  });

  afterEach(async () => {
    global.Date = DateOrigin;

    jest.resetAllMocks();
  });

  afterAll(async () => {
    await setup.teardownDb(mongodb);
  });

  it('allows to create a new instance', async () => {
    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    await room.create('King', 5);

    const { currentState } = await Rooms.getState(db, room.correlationId);

    expect(currentState).toEqual({
      room_id: roomId,
      version: 0,
      name: 'King',
      quantity: 5,
    });
  });

  it('allows to create a new instance without a logger', async () => {
    const room = new Rooms({ db }, roomId); // 1 is the correlation id

    await room.create('King', 5);

    const { currentState } = await Rooms.getState(db, room.correlationId);

    expect(currentState).toEqual({
      room_id: roomId,
      version: 0,
      name: 'King',
      quantity: 5,
    });
  });

  it('blocks one of two concurrent requests', async () => {
    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    await room.create('King', 1);

    let error;

    try {
      await Promise.all([room.book(1), room.book(1)]);
    } catch (err) {
      error = err;
    }

    expect(error).not.toEqual(null);

    const { currentState } = await Rooms.getState(db, room.correlationId);

    expect(currentState).toEqual({
      room_id: roomId,
      version: 1,
      name: 'King',
      quantity: 0,
    });
  });

  it('updates properly the state with the missing events that should be played', async () => {
    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    // Create room state
    const stateCreation = await room.create('King', 5);

    // Make a booking and create associated event
    await room.book(1);

    // Set back the inital creation state to the room in DB
    await Rooms.getStatesCollection(db).findOneAndReplace(
      { room_id: room.correlationId },
      stateCreation,
    );

    // Get the built state from current state in db and playing missing events
    const { currentState } = await Rooms.getState(db, room.correlationId);
    expect(currentState).toEqual({
      room_id: roomId,
      version: 1,
      name: 'King',
      quantity: 4,
    });

    // Actual state of the room in DB
    const actualStateBeforeSecondBooking = await Rooms.getStatesCollection(
      db,
    ).findOne(
      {
        room_id: room.correlationId,
      },
      { projection: { _id: 0 } },
    );

    expect(actualStateBeforeSecondBooking).toEqual({
      room_id: roomId,
      version: 0,
      name: 'King',
      quantity: 5,
    });

    // Make another booking
    const actualStateAfterSecondBooking = await room.book(1);

    expect(actualStateAfterSecondBooking).toEqual({
      room_id: roomId,
      version: 2,
      name: 'King',
      quantity: 3,
    });
  });

  it('updates properly the state with the missing events in an optimistic state storing option', async () => {
    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    // Create room state
    await room.create('King', 5, {
      mustWaitStatePersistence: false,
    });

    const { currentState: s0 } = await Rooms.getState(db, room.correlationId);
    expect(s0).toMatchObject({
      quantity: 5,
    });

    // Make a booking and create associated event
    await room.book(1, false, 0, () => null, {
      mustWaitStatePersistence: false,
    });
    const { currentState: s1 } = await Rooms.getState(db, room.correlationId);
    expect(s1).toMatchObject({
      quantity: 4,
    });

    await room.book(1, false, 0, () => null, {
      mustWaitStatePersistence: false,
    });
    const { currentState: s2 } = await Rooms.getState(db, room.correlationId);
    expect(s2).toMatchObject({
      quantity: 3,
    });

    const actualStateAfterBookings = await Rooms.getStatesCollection(
      db,
    ).findOne(
      {
        room_id: room.correlationId,
      },
      { projection: { _id: 0 } },
    );
    expect(actualStateAfterBookings.quantity).toBeGreaterThanOrEqual(3);
  });

  it('stores internally latest handled events', async () => {
    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    // Create room state
    await room.create('King', 5, {
      mustWaitStatePersistence: false,
    });

    const { currentState: s0 } = await Rooms.getState(db, room.correlationId);
    expect(s0).toMatchObject({
      quantity: 5,
    });

    // Make a booking and create associated event
    await room.book(1, false, 0, () => null, {
      mustWaitStatePersistence: false,
    });

    expect(room.latestHandledEvents).toMatchObject([
      {
        name: 'King',
        quantity: 5,
        room_id: room.correlationId,
        type: 'CREATED',
        version: 0,
      },
      {
        quantity: 1,
        room_id: room.correlationId,
        type: 'ROOMS_BOOKED',
        version: 1,
      },
    ]);
  });

  it('blocks a request for business logic reason (no more room)', async () => {
    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    await room.create('King', 1);
    await room.book(1);

    let error;

    try {
      await room.book(1);
    } catch (err) {
      error = err;
    }

    expect(error).toBeInstanceOf(Error);
    expect(error).toHaveProperty('message', 'No more room');
  });

  it('retries requests if handler invoked with retry', async () => {
    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    await room.create('King', 2);

    await Promise.all([room.book(1, true, 100), room.book(1, true, 100)]);

    const { currentState } = await Rooms.getState(db, room.correlationId);

    expect(currentState).toEqual({
      room_id: roomId,
      version: 2,
      name: 'King',
      quantity: 0,
    });
  });

  it('throws an error if retry delay has been reached', async () => {
    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    await room.create('King', 1);

    let error;

    try {
      await Promise.all([room.book(1, true, 100), room.book(1, true, 100)]);
    } catch (err) {
      error = err;
    }

    expect(error).toBeInstanceOf(Error);
    expect(error).toHaveProperty('message', 'Retry duration reached');

    const { currentState } = await Rooms.getState(db, room.correlationId);

    expect(currentState).toEqual({
      room_id: roomId,
      version: 1,
      name: 'King',
      quantity: 0,
    });
  });

  it('throws an error retryDuration is not a number', async () => {
    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    await room.create('King', 1);

    let error;

    try {
      await Promise.all([room.book(1, true, null), room.book(1, true, null)]);
    } catch (err) {
      error = err;
    }

    expect(error).toBeInstanceOf(AssertionError);
    expect(error).toHaveProperty('message', 'retryDuration must be a number');
  });

  it('throws an error if storeStateErrorHandler is not a function', async () => {
    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    await room.create('King', 1);

    let error;

    try {
      await Promise.all([room.book(1, false, 100, 'storeStateErrorHandler')]);
    } catch (err) {
      error = err;
    }

    expect(error).toBeInstanceOf(AssertionError);
    expect(error).toHaveProperty(
      'message',
      'storeStateErrorHandler must be a function',
    );
  });

  it('should call the defaultStoreStateErrorHandler on failure if none is given', async () => {
    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    await room.create('King', 1);

    const error = new Error('Failure');

    // provoke a failure a store state failure
    const storeStateStub = jest
      .spyOn(Object.getPrototypeOf(Rooms), 'storeState')
      .mockRejectedValue(error);
    const loggerErrorSpy = jest.spyOn(logger, 'info');

    await room.book(1, true, 100);

    expect(storeStateStub.mock.calls[0][1]).toEqual({
      name: 'King',
      quantity: 0,
      version: 1,
      room_id: roomId,
    });
    expect(loggerErrorSpy.mock.calls).toEqual([
      [
        {
          err: error,
          updatedState: {
            name: 'King',
            quantity: 0,
            room_id: roomId,
            version: 1,
          },
        },
        'Error occured when attempted to save the state',
      ],
    ]);
  });

  it('should call storeStateErrorHandler on store state error (handlewithretry case)', async () => {
    const storeStateErrorHandler = jest.fn();

    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    await room.create('King', 1);

    const storeError = new Error('Failure');

    // provoke a failure a store state failure
    const storeStateStub = jest
      .spyOn(Object.getPrototypeOf(Rooms), 'storeState')
      .mockRejectedValue(storeError);

    await room.book(1, true, 100, storeStateErrorHandler);

    expect(storeStateStub.mock.calls[0][1]).toEqual({
      name: 'King',
      quantity: 0,
      version: 1,
      room_id: roomId,
    });
    expect(storeStateErrorHandler).toHaveBeenCalledWith(storeError, {
      name: 'King',
      quantity: 0,
      room_id: roomId,
      version: 1,
    });
  });

  it('should call storeStateErrorHandler on store state error (handle case)', async () => {
    const storeStateErrorHandler = jest.fn();

    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    await room.create('King', 1);

    const storeError = new Error('Failure');

    // provoke a failure a store state failure
    const storeStateStub = jest
      .spyOn(Object.getPrototypeOf(Rooms), 'storeState')
      .mockRejectedValue(storeError);

    const state = await room.book(1, false, null, storeStateErrorHandler);

    expect(storeStateStub.mock.calls[0][1]).toEqual({
      name: 'King',
      quantity: 0,
      version: 1,
      room_id: roomId,
    });

    expect(storeStateErrorHandler).toHaveBeenCalledWith(storeError, {
      name: 'King',
      quantity: 0,
      room_id: roomId,
      version: 1,
    });
    expect(state).toEqual({
      name: 'King',
      quantity: 0,
      room_id: roomId,
      version: 1,
    });
  });

  it('should throw a readonly failed exception if the current state is flagged as readonly', async () => {
    const storeStateErrorHandler = jest.fn();

    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    const _state = await room.create('King', 1);

    const updatedState = await room.handle(() => [
      {
        type: 'IS_READONLY',
        is_readonly: true,
      },
    ]);

    let error;

    try {
      const state = await room.book(1, false, null, storeStateErrorHandler, {
        imperativeVersion: 2,
        isReadonly: 'is_readonly',
      });
    } catch (err) {
      error = err;
    }

    expect(error).toEqual(Rooms.ERRORS.ENTITY_IS_READONLY);
  });

  it('should not block entity creation with isReadonly flag', async () => {
    const storeStateErrorHandler = jest.fn();

    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    const state = await room.handle(
      () => [
        {
          type: 'CREATED',
          name: 'King',
          quantity: 1,
        },
      ],
      () => null,
      {
        isReadonly: 'is_readonly',
      },
    );

    expect(state).toMatchObject({
      name: 'King',
      quantity: 1,
      version: 0,
    });
  });

  it('should throw an imperative condition failed exception if the event version does not match the expected one', async () => {
    const storeStateErrorHandler = jest.fn();

    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    const _state = await room.create('King', 1);

    let error;

    try {
      const state = await room.book(1, false, null, storeStateErrorHandler, {
        imperativeVersion: 2,
      });
    } catch (err) {
      error = err;
    }

    expect(error).toEqual(Rooms.ERRORS.HANDLER_IMPERATIVE_CONDITION_FAILED);
  });

  it('should throw an imperative condition failed exception if the event version does not match the expected one', async () => {
    const storeStateErrorHandler = jest.fn();

    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    await room.create('King', 1);

    const loops = 10;
    const parallel = 10;

    for (let i = 0; i < loops; i++) {
      const requests = new Array(parallel).fill(1).map(() =>
        room.book(0, false, null, storeStateErrorHandler, {
          imperativeVersion: 1 + i,
        }),
      );

      const results = await Promise.allSettled(requests);

      const success = results.filter((r) => r.status === 'fulfilled');
      const error = results.filter((r) => r.status === 'rejected');
      expect(success.length).toEqual(1);
      expect(error.length).toEqual(parallel - 1);

      const events = await Rooms.getEvents(db, roomId, -1).toArray();
      expect(events.length).toEqual(i + 2);
    }
  });

  it('should validate an imperative condition if requested', async () => {
    const storeStateErrorHandler = jest.fn();

    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    await room.create('King', 1);

    const state = await room.book(1, false, null, storeStateErrorHandler);

    expect(state).toMatchObject({
      version: 1,
    });
  });

  it('throws an error if the handler is trying to mutate the object state', async () => {
    const room = new Rooms({ db, logger }, roomId); // 1 is the correlation id

    const state = await room.create('King', 1);

    let error;

    try {
      state.test = 1;
    } catch (err) {
      error = err;
    }

    expect(error).toBeInstanceOf(TypeError);
    expect(error).toHaveProperty(
      'message',
      'Cannot add property test, object is not extensible',
    );
  });

  it('allows to snapshot the state', async () => {
    const room = new Rooms({ db, logger }, roomId); // 2 is the correlation id

    const state = await room.create('King', 1, {
      mustWaitStatePersistence: true,
    });

    const snapshot = await room.createSnapshot();

    expect(snapshot).toEqual(state);
  });

  it('allows to get a version in the past', async () => {
    const room = new Rooms({ db, logger }, roomId); // 2 is the correlation id

    await room.create('King', 10);

    await Promise.all([
      room.book(1, true, 1000),
      room.book(1, true, 1000),
      room.book(1, true, 1000),
      room.book(1, true, 1000),
      room.book(1, true, 1000),
    ]);

    const { currentState } = await Rooms.getState(db, room.correlationId);
    expect(currentState).toMatchObject({
      version: 5,
      quantity: 5,
    });

    expect(
      await Rooms.getStateAtVersion(db, room.correlationId, 0),
    ).toMatchObject({
      version: 0,
      quantity: 10,
    });

    expect(
      await Rooms.getStateAtVersion(db, room.correlationId, 2),
    ).toMatchObject({
      version: 2,
      quantity: 8,
    });
  });

  it('allows to get a version in the past based on snapshot', async () => {
    const room = new Rooms({ db, logger }, roomId); // 2 is the correlation id

    await room.create('King', 10);

    await Promise.all([
      room.book(1, true, 1000),
      room.book(1, true, 1000),
      room.book(1, true, 1000),
    ]);

    await room.createSnapshot();

    await Promise.all([room.book(1, true, 1000), room.book(1, true, 1000)]);

    const { currentState } = await Rooms.getState(db, room.correlationId);
    expect(currentState).toMatchObject({
      version: 5,
      quantity: 5,
    });

    expect(
      await Rooms.getStateAtVersion(db, room.correlationId, 0),
    ).toMatchObject({
      version: 0,
      quantity: 10,
    });

    expect(
      await Rooms.getStateAtVersion(db, room.correlationId, 5),
    ).toMatchObject({
      version: 5,
      quantity: 5,
    });
  });

  it('allows to get a version in the past based on an exact snapshot match', async () => {
    const room = new Rooms({ db, logger }, roomId); // 2 is the correlation id

    Rooms.getEvents = jest.fn();

    await room.create('King', 10);

    await Promise.all([
      room.book(1, true, 1000),
      room.book(1, true, 1000),
      room.book(1, true, 1000),
    ]);

    await room.createSnapshot();

    expect(Rooms.getEvents).toHaveBeenCalledTimes(0);
    expect(
      await Rooms.getStateAtVersion(db, room.correlationId, 3),
    ).toMatchObject({
      version: 3,
      quantity: 7,
    });
  });

  it('throws an error if the version can not be reached', async () => {
    const room = new Rooms({ db, logger }, roomId); // 2 is the correlation id

    await room.create('King', 10);

    await Promise.all([
      room.book(1, true, 1000),
      room.book(1, true, 1000),
      room.book(1, true, 1000),
    ]);

    let error;

    try {
      await Rooms.getStateAtVersion(db, room.correlationId, 5);
    } catch (err) {
      error = err;
    }

    expect(error).toBeInstanceOf(Error);
    expect(error.message).toBe('State version does not exist');
  });

  it('returns the closest version if mustThrow is set to true', async () => {
    const room = new Rooms({ db, logger }, roomId); // 2 is the correlation id

    await room.create('King', 10);

    await Promise.all([
      room.book(1, true, 1000),
      room.book(1, true, 1000),
      room.book(1, true, 1000),
    ]);

    const stateInPast = await Rooms.getStateAtVersion(
      db,
      room.correlationId,
      5,
      false,
    );
    expect(stateInPast).toMatchObject({
      version: 3,
      quantity: 7,
    });
  });

  describe('with global version', () => {
    class RoomsGlobal extends EventSourcedFactory('rooms', reducer, {
      CORRELATION_FIELD: 'room_id',
      WITH_GLOBAL_VERSION: true,
    }) {}

    it('allows to create a new instance at version 0', async () => {
      const room = new RoomsGlobal({ db, logger }, roomId); // 1 is the correlation id

      await room.handle(
        () => [
          {
            type: 'CREATED',
            name: 'King',
            quantity: 1,
          },
        ],
        () => null,
      );

      const { currentState } = await Rooms.getState(db, room.correlationId);

      expect(currentState).toEqual({
        room_id: roomId,
        version: 0,
        name: 'King',
        quantity: 1,
      });
    });

    it('allows to create 2 different instances with consecutive versions', async () => {
      const roomA = new RoomsGlobal({ db, logger }, roomId);
      const roomB = new RoomsGlobal({ db, logger }, roomId);

      await roomA.handle(
        () => [
          {
            type: 'CREATED',
            name: 'King',
            quantity: 1,
          },
        ],
        () => null,
      );
      await roomB.handle(
        () => [
          {
            type: 'CREATED',
            name: 'King',
            quantity: 1,
          },
        ],
        () => null,
      );

      const { currentState } = await Rooms.getState(db, roomB.correlationId);

      expect(currentState).toEqual({
        room_id: roomId,
        version: 1,
        name: 'King',
        quantity: 1,
      });
    });

    it('stores events in consecutive orders globally', async () => {
      const roomA = new RoomsGlobal(
        { db, logger },
        Math.floor(setup.random() * 1e9),
      );
      const roomB = new RoomsGlobal(
        { db, logger },
        Math.floor(setup.random() * 1e9),
      );

      await roomA.handle(
        () => [
          {
            type: 'CREATED',
            name: 'King',
            quantity: 1,
          },
        ],
        () => null,
      );
      await roomB.handle(
        () => [
          {
            type: 'CREATED',
            name: 'King',
            quantity: 1,
          },
        ],
        () => null,
      );
      await roomA.handle(
        () => [
          {
            type: 'ROOMS_BOOKED',
            quantity: 1,
          },
        ],
        () => null,
      );

      const { currentState } = await Rooms.getState(db, roomA.correlationId);

      expect(currentState).toEqual({
        room_id: roomA.correlationId,
        version: 2,
        name: 'King',
        quantity: 0,
      });

      const events = await Rooms.getEventsCollection(db).find().toArray();

      expect(events).toMatchObject([
        {
          room_id: roomA.correlationId,
          version: 0,
        },
        {
          room_id: roomB.correlationId,
          version: 1,
        },
        {
          room_id: roomA.correlationId,
          version: 2,
        },
      ]);
    });

    it('allows to get state at a given version', async () => {
      const roomA = new RoomsGlobal(
        { db, logger },
        Math.floor(setup.random() * 1e9),
      );
      const roomB = new RoomsGlobal(
        { db, logger },
        Math.floor(setup.random() * 1e9),
      );

      await roomA.handle(
        () => [
          {
            type: 'CREATED',
            name: 'King',
            quantity: 2,
          },
        ],
        () => null,
      );
      await roomB.handle(
        () => [
          {
            type: 'CREATED',
            name: 'King',
            quantity: 1,
          },
        ],
        () => null,
      );
      await roomA.handle(
        () => [
          {
            type: 'ROOMS_BOOKED',
            quantity: 1,
          },
        ],
        () => null,
      );
      await roomB.handle(
        () => [
          {
            type: 'ROOMS_BOOKED',
            quantity: 1,
          },
        ],
        () => null,
      );
      await roomA.handle(
        () => [
          {
            type: 'ROOMS_BOOKED',
            quantity: 1,
          },
        ],
        () => null,
      );

      const lastVersion = await RoomsGlobal.getStateAtVersion(
        db,
        roomA.correlationId,
        30,
      );

      expect(lastVersion).toEqual({
        room_id: roomA.correlationId,
        version: 4,
        name: 'King',
        quantity: 0,
      });

      const stateVersion2 = await RoomsGlobal.getStateAtVersion(
        db,
        roomA.correlationId,
        3,
      );

      expect(stateVersion2).toEqual({
        room_id: roomA.correlationId,
        version: 2,
        name: 'King',
        quantity: 1,
      });

      const events = await RoomsGlobal.getEventsCollection(db).find().toArray();

      expect(events).toMatchObject([
        {
          room_id: roomA.correlationId,
          version: 0,
        },
        {
          room_id: roomB.correlationId,
          version: 1,
        },
        {
          room_id: roomA.correlationId,
          version: 2,
        },
        {
          room_id: roomB.correlationId,
          version: 3,
        },
        {
          room_id: roomA.correlationId,
          version: 4,
        },
      ]);
    });

    it('supports retry with global version', async () => {
      await RoomsGlobal.initIndexes(db);

      const rooms = [
        new RoomsGlobal({ db, logger }, Math.floor(setup.random() * 1e9)),
        new RoomsGlobal({ db, logger }, Math.floor(setup.random() * 1e9)),
        new RoomsGlobal({ db, logger }, Math.floor(setup.random() * 1e9)),
        new RoomsGlobal({ db, logger }, Math.floor(setup.random() * 1e9)),
        new RoomsGlobal({ db, logger }, Math.floor(setup.random() * 1e9)),
      ];

      await Promise.all(
        rooms.map((r) =>
          r.handleWithRetry(
            () => [
              {
                type: 'CREATED',
                name: 'King',
                quantity: 1,
              },
            ],
            1000,
            () => null,
          ),
        ),
      );

      const storedRooms = await RoomsGlobal.getStatesCollection(db)
        .find()
        .toArray();

      expect(storedRooms).toMatchObject([
        {
          version: 0,
        },
        {
          version: 1,
        },
        {
          version: 2,
        },
        {
          version: 3,
        },
        {
          version: 4,
        },
      ]);
    });
  });

  describe('with blockchain', () => {
    class RoomsGlobalBlockChain extends EventSourcedFactory('rooms', reducer, {
      CORRELATION_FIELD: 'room_id',
      WITH_GLOBAL_VERSION: true,
      WITH_BLOCKCHAIN_HASH: true,
    }) {}

    beforeEach(() => {
      jest.spyOn(date, 'getNow').mockImplementation(() => new Date(2021, 0, 1));
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('allows to create a new instance at version 0', async () => {
      const room = new RoomsGlobalBlockChain({ db, logger }, '123456');

      await room.handle(
        () => [
          {
            type: 'CREATED',
            name: 'King',
            quantity: 1,
          },
        ],
        () => null,
      );

      const { currentState } = await RoomsGlobalBlockChain.getState(
        db,
        room.correlationId,
      );

      expect(currentState).toEqual({
        room_id: '123456',
        version: 0,
        name: 'King',
        quantity: 1,
        hash: '3fba3ee8c286d66114b0ffc2e9fe68ced50294a68ff589c77a3da2c6a32680fa',
        nonce: 0,
      });

      const [event] = await RoomsGlobalBlockChain.getEventsCollection(db)
        .find(
          {
            room_id: room.correlationId,
          },
          {
            projection: {
              _id: 0,
            },
          },
        )
        .toArray();

      const hash = RoomsGlobalBlockChain.computeHash(
        event.version,
        event.created_at,
        event,
        event.nonce,
        event.prev,
      );

      expect(hash).toEqual(event.hash);
    });

    it('allows to create 2 different instances with consecutive versions', async () => {
      const roomA = new RoomsGlobalBlockChain({ db, logger }, 'a_123456');
      const roomB = new RoomsGlobalBlockChain({ db, logger }, 'b_123456');

      await roomA.handle(
        () => [
          {
            type: 'CREATED',
            name: 'King',
            quantity: 1,
          },
        ],
        () => null,
      );
      await roomB.handle(
        () => [
          {
            type: 'CREATED',
            name: 'King',
            quantity: 1,
          },
        ],
        () => null,
      );

      const { currentState } = await RoomsGlobalBlockChain.getState(
        db,
        roomB.correlationId,
      );

      expect(currentState).toEqual({
        room_id: 'b_123456',
        version: 1,
        name: 'King',
        quantity: 1,
        hash: '53f7684270326acfe6c2bc626c3f8f8216eb61e071ff33c7ea150b795816f7f4',
        nonce: 0,
      });
    });
  });
});
