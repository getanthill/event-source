import { EventSourcedFactory } from '../../../src/EventSourced';

import setup from '../../setup';

describe('EventSourced', () => {
  describe('static', () => {
    describe('getStatesCollection', () => {
      let mongodb;
      let db;

      beforeAll(async () => {
        mongodb = await setup.initDb();
        db = mongodb.DB;
      });

      afterAll(async () => {
        await setup.teardownDb(mongodb);
      });

      it('returns the states collection', () => {
        class Users extends EventSourcedFactory('users', () => null) {}

        const collection = Users.getStatesCollection(db);

        expect(collection).toHaveProperty('collectionName', 'users');
      });
    });
  });
});
