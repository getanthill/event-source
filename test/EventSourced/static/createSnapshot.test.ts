import { EventSourcedFactory as EventSourced } from '../../../src/EventSourced';

import setup from '../../setup';

describe('EventSourced', () => {
  describe('static', () => {
    describe('createSnapshot', () => {
      class Users extends EventSourced('users', () => null) {}

      let mongodb;
      let db;

      beforeAll(async () => {
        mongodb = await setup.initDb();
        db = mongodb.DB;
      });

      beforeEach(async () => {
        await Promise.all([
          Users.getStatesCollection(db).deleteMany(),
          Users.getEventsCollection(db).deleteMany(),
          Users.getSnapshotsCollection(db).deleteMany(),
          Users.initIndexes(db),
        ]);
      });

      afterAll(async () => {
        await setup.teardownDb(mongodb);
      });

      it('creates a new snapshots from the existing state', async () => {
        await Users.storeState(
          db,
          {
            correlation_id: 1,
            version: 0,
          },
          -1,
        );

        const { currentState } = await Users.getState(db, 1);

        const snapshot = await Users.createSnapshot(db, 1);

        expect(currentState).toEqual(snapshot);
      });
    });
  });
});
