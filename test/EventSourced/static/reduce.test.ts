import { AssertionError } from 'assert';

import { EventSourcedFactory } from '../../../src/EventSourced';

import * as date from '../../../src/utils/date';

describe('EventSourced', () => {
  let DateOrigin = Date;
  let constantDate;

  describe('static', () => {
    beforeEach(() => {
      constantDate = new Date('2020-11-10T00:00:00.000Z');

      // @ts-ignore
      global.Date = class extends Date {
        constructor() {
          super();

          return constantDate;
        }
      };
    });

    afterEach(() => {
      global.Date = DateOrigin;
    });

    describe('reduce', () => {
      const INITIAL_STATE = {};

      const reducer = (state, event) => {
        if (event.type === 'CREATED') {
          return { ...INITIAL_STATE };
        }

        if (event.type === 'FIRSTNAME_UPDATED') {
          return {
            ...state,
            firstname: event.firstname,
          };
        }

        return state;
      };

      class Users extends EventSourcedFactory('users', reducer) {}

      class UsersGlobal extends EventSourcedFactory('users', reducer, {
        WITH_GLOBAL_VERSION: true,
      }) {}

      class UsersBlockChain extends EventSourcedFactory('users', reducer, {
        WITH_BLOCKCHAIN_HASH: true,
        BLOCKCHAIN_HASH_DIFFICULTY: 1,
      }) {}

      class UsersGlobalBlockChain extends EventSourcedFactory(
        'users',
        reducer,
        {
          WITH_GLOBAL_VERSION: true,
          WITH_BLOCKCHAIN_HASH: true,
          BLOCKCHAIN_HASH_DIFFICULTY: 1,
        },
      ) {}

      beforeEach(() => {
        jest.spyOn(date, 'getNow').mockImplementation(() => constantDate);
      });

      afterEach(() => {
        jest.resetAllMocks();
      });

      it('returns an array with the state and the mapped events', async () => {
        const originalEvents = [
          {
            type: 'FIRSTNAME_UPDATED',
            firstname: 'John',
          },
        ];

        const originalState = {
          version: 0,
        };

        const [updatedState, updatedEvents] = await Users.reduce(
          originalEvents,
          originalState,
          '1',
        );

        expect(originalState).toEqual({
          version: 0,
        });

        expect(updatedState).toEqual({
          correlation_id: '1',
          version: 1,
          firstname: 'John',
        });

        expect(originalEvents).toEqual([
          {
            type: 'FIRSTNAME_UPDATED',
            firstname: 'John',
          },
        ]);

        expect(updatedEvents).toEqual([
          {
            created_at: constantDate,
            type: 'FIRSTNAME_UPDATED',
            correlation_id: '1',
            firstname: 'John',
            version: 1,
          },
        ]);
      });

      it('returns the updated state', async () => {
        const [state] = await Users.reduce(
          [
            {
              type: 'CREATED',
            },
          ],
          undefined,
          '1',
        );
        expect(state).toEqual({
          correlation_id: '1',
          version: 0,
        });
      });

      it('returns the updated state with version from lastEvent', async () => {
        const [state] = await UsersGlobal.reduce(
          [
            {
              type: 'CREATED',
            },
          ],
          undefined,
          '1',
          {
            version: 1,
          },
        );
        expect(state).toEqual({
          correlation_id: '1',
          version: 2,
        });
      });

      it('returns the updated state with version from lastEvent and multiple events', async () => {
        const [state] = await UsersGlobal.reduce(
          [
            {
              type: 'FIRSTNAME_UPDATED',
              firstname: 'John',
            },
            {
              type: 'FIRSTNAME_UPDATED',
              firstname: 'Doe',
            },
          ],
          undefined,
          '1',
          {
            version: 1,
          },
        );
        expect(state).toEqual({
          correlation_id: '1',
          firstname: 'Doe',
          version: 3,
        });
      });

      it('returns the updated state with a hash attached (genesis)', async () => {
        const [state, events] = await UsersBlockChain.reduce(
          [
            {
              type: 'FIRSTNAME_UPDATED',
              firstname: 'John',
            },
          ],
          undefined,
          '1',
        );

        expect(state).toEqual({
          correlation_id: '1',
          firstname: 'John',
          version: 0,
          hash: '0d3c5bb937557cb977620f74fe6b50607ecbe23d26e4f5016c6d1c2d7b7223cc',
          nonce: 6,
        });

        expect(events).toEqual([
          {
            created_at: constantDate,
            type: 'FIRSTNAME_UPDATED',
            correlation_id: '1',
            firstname: 'John',
            version: 0,
            hash: '0d3c5bb937557cb977620f74fe6b50607ecbe23d26e4f5016c6d1c2d7b7223cc',
            nonce: 6,
            prev: '0000000000000000000000000000000000000000000000000000000000000000',
          },
        ]);

        const hash = UsersBlockChain.computeHash(
          events[0].version,
          events[0].created_at,
          events[0],
          events[0].nonce,
          events[0].prev,
        );

        expect(hash).toEqual(events[0].hash);
      });

      it('returns the updated state with a hash attached', async () => {
        const [state] = await UsersBlockChain.reduce(
          [
            {
              type: 'FIRSTNAME_UPDATED',
              firstname: 'John',
            },
            {
              type: 'FIRSTNAME_UPDATED',
              firstname: 'Doe',
            },
          ],
          undefined,
          '1',
        );

        expect(state).toEqual({
          correlation_id: '1',
          firstname: 'Doe',
          version: 1,
          hash: '06afe75ab1c5ae58df59776851d9855c13cda7486e957eba5321e8fcc627f93c',
          nonce: 6,
        });
      });

      it('returns the updated state with a hash attached (global version)', async () => {
        const [state, events] = await UsersGlobalBlockChain.reduce(
          [
            {
              type: 'FIRSTNAME_UPDATED',
              firstname: 'John',
            },
            {
              type: 'FIRSTNAME_UPDATED',
              firstname: 'Doe',
            },
          ],
          undefined,
          '1',
          {
            version: 1,
            hash: '1111111111111111111111111111111111111111111111111111111111111111',
          },
        );

        expect(state).toEqual({
          correlation_id: '1',
          firstname: 'Doe',
          version: 3,
          hash: '08f35ec681e606dcbd7a653c7b47fdee5ed1fa305c4e3c1c92dd9af883ade8f6',
          nonce: 5,
        });

        expect(events).toMatchObject([
          {
            version: 2,
            prev: '1111111111111111111111111111111111111111111111111111111111111111',
            hash: '0e6f39ba25d4cf479aa9a065547452dca11f4a153a62f8e4d63f9ebc87124162',
          },
          {
            version: 3,
            prev: '0e6f39ba25d4cf479aa9a065547452dca11f4a153a62f8e4d63f9ebc87124162',
            hash: '08f35ec681e606dcbd7a653c7b47fdee5ed1fa305c4e3c1c92dd9af883ade8f6',
          },
        ]);
      });

      it('throws an error if an event to reduce has a version number not being the next one', async () => {
        const originalEvents = [
          {
            type: 'FIRSTNAME_UPDATED',
            firstname: 'John',
            version: 1, // Valid version
          },
          {
            type: 'FIRSTNAME_UPDATED',
            firstname: 'Doe',
            version: 3, // Invalid version (gap)
          },
        ];

        const originalState = {
          version: 0,
        };

        let error;

        try {
          await Users.reduce(originalEvents, originalState, '1');
        } catch (err) {
          error = err;
        }

        expect(error).toEqual(
          Users.ERRORS.INCOMPATIBLE_EVENT_VERSION_WITH_STATE_VERSION,
        );
      });

      it('throws an error if no correlation id is provided', async () => {
        let error;

        try {
          // @ts-ignore
          await Users.reduce();
        } catch (err) {
          error = err;
        }

        expect(error).toBeInstanceOf(AssertionError);
        expect(error).toHaveProperty(
          'message',
          '[EventSourced#reduce] correlationId must be defined',
        );
      });
    });
  });
});
