import { EventSourcedFactory } from '../../../src/EventSourced';

import setup from '../../setup';

describe('EventSourced', () => {
  describe('static', () => {
    describe.only('getState', () => {
      const INITIAL_STATE = {};

      class Users extends EventSourcedFactory('users', (state, event) => {
        if (event.type === 'CREATED') {
          return INITIAL_STATE;
        }

        if (event.type === 'FIRSTNAME_UPDATED') {
          return {
            ...state,
            firstname: event.firstname,
          };
        }

        return state;
      }) {}

      let mongodb;
      let db;

      beforeAll(async () => {
        mongodb = await setup.initDb();
        db = mongodb.DB;
      });

      beforeEach(async () => {
        await Promise.all([
          Users.getStatesCollection(db).deleteMany(),
          Users.getEventsCollection(db).deleteMany(),
          Users.getSnapshotsCollection(db).deleteMany(),
        ]);
      });

      afterAll(async () => {
        await setup.teardownDb(mongodb);
      });

      it('returns null if no state nor event have been found', async () => {
        const state = await Users.getState(db, '1');

        expect(state).toEqual({ stateInDb: null, currentState: null });
      });

      it('returns object with stateInDb property that is the actual state in db if available in the collection', async () => {
        await Users.getStatesCollection(db).insertOne({
          correlation_id: '1',
          version: 0,
        });

        const { stateInDb } = await Users.getState(db, '1');

        expect(stateInDb).not.toEqual(null);
        expect(stateInDb).toHaveProperty('correlation_id', '1');
        expect(stateInDb).toHaveProperty('version', 0);
      });

      it('returns object with currentState property that is the last available state with the reduced events', async () => {
        await Users.getEventsCollection(db).insertOne({
          type: 'CREATED',
          correlation_id: '1',
          version: 0,
        });

        const { currentState } = await Users.getState(db, '1');

        expect(currentState).not.toEqual(null);
        expect(currentState).toHaveProperty('correlation_id', '1');
        expect(currentState).toHaveProperty('version', 0);
      });

      it('returns the actual state in db and the last available state with the reduced events', async () => {
        await Users.getStatesCollection(db).insertOne({
          correlation_id: '1',
          version: 0,
        });

        await Users.getEventsCollection(db).insertOne({
          type: 'CREATED',
          correlation_id: '1',
          version: 0,
        });

        await Users.getEventsCollection(db).insertOne({
          type: 'FIRSTNAME_UPDATED',
          correlation_id: '1',
          version: 1,
          firstname: 'John',
        });

        const { stateInDb, currentState } = await Users.getState(db, '1');

        expect({ stateInDb, currentState }).toEqual({
          currentState: {
            correlation_id: '1',
            firstname: 'John',
            version: 1,
          },
          stateInDb: {
            correlation_id: '1',
            version: 0,
          },
        });
      });
    });
  });
});
