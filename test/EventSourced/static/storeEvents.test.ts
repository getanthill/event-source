import { EventSourcedFactory } from '../../../src/EventSourced';

import setup from '../../setup';

describe('EventSourced', () => {
  describe('static', () => {
    describe('storeEvents', () => {
      class Users extends EventSourcedFactory('users', () => null) {}

      let mongodb;
      let db;

      beforeAll(async () => {
        mongodb = await setup.initDb();
        db = mongodb.DB;
      });

      beforeEach(async () => {
        await Promise.all([
          Users.getStatesCollection(db).deleteMany(),
          Users.getEventsCollection(db).deleteMany(),
          Users.getSnapshotsCollection(db).deleteMany(),
        ]);
      });

      afterAll(async () => {
        await setup.teardownDb(mongodb);
      });

      it('stores events to the events collection', async () => {
        await Users.storeEvents(db, [
          {
            correlation_id: 1,
            version: 0,
          },
          {
            correlation_id: 1,
            version: 1,
          },
        ]);

        const events = await Users.getEvents(db, 1, -1).toArray();

        expect(events).toHaveProperty('length', 2);
      });

      it('stores events to the events collection with an embedded created_at date', async () => {
        await Users.storeEvents(db, [
          {
            created_at: new Date('2021-01-01'),
            correlation_id: 1,
            version: 0,
          },
          {
            created_at: new Date('2021-02-01'),
            correlation_id: 1,
            version: 1,
          },
        ]);

        const events = await Users.getEventsCollection(db).find().toArray();

        expect(events).toMatchObject([
          {
            created_at: new Date('2021-01-01'),
            version: 0,
          },
          {
            created_at: new Date('2021-02-01'),
            version: 1,
          },
        ]);
      });
    });
  });
});
