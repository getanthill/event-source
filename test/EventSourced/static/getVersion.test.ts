import { EventSourcedFactory } from '../../../src/EventSourced';

describe('EventSourced', () => {
  describe('static', () => {
    describe('getVersion', () => {
      it('returns the state version', () => {
        class Users extends EventSourcedFactory('users', () => null) {}

        expect(
          Users.getVersion({
            version: 1,
          }),
        ).toEqual(1);
      });

      it('returns -1 if not available', () => {
        class Users extends EventSourcedFactory('users', () => null) {}

        expect(Users.getVersion({})).toEqual(-1);
      });
    });
  });
});
