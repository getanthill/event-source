import { EventSourcedFactory } from '../../../src/EventSourced';

import setup from '../../setup';

describe('EventSourced', () => {
  describe('static', () => {
    describe('storeState', () => {
      class Users extends EventSourcedFactory('users', () => null) {}

      let mongodb;
      let db;

      beforeAll(async () => {
        mongodb = await setup.initDb();
        db = mongodb.DB;
      });

      beforeEach(async () => {
        await Promise.all([
          Users.getStatesCollection(db).deleteMany(),
          Users.getEventsCollection(db).deleteMany(),
          Users.getSnapshotsCollection(db).deleteMany(),
          Users.initIndexes(db),
        ]);
      });

      afterAll(async () => {
        await setup.teardownDb(mongodb);
      });

      it('stores a new state into the states collection', async () => {
        await Users.storeState(
          db,
          {
            correlation_id: 1,
            version: 0,
          },
          -1,
        );

        const { currentState } = await Users.getState(db, 1);

        expect(currentState).toHaveProperty('correlation_id', 1);
        expect(currentState).toHaveProperty('version', 0);
      });

      it('updates the state present in database if already exists', async () => {
        await Users.storeState(
          db,
          {
            correlation_id: 1,
            version: 0,
          },
          -1,
        );

        await Users.storeState(
          db,
          {
            correlation_id: 1,
            version: 1,
          },
          0,
        );

        const { currentState } = await Users.getState(db, 1);

        expect(currentState).toHaveProperty('correlation_id', 1);
        expect(currentState).toHaveProperty('version', 1);
      });

      it('throws an error if trying to store a state from invalid version', async () => {
        await Users.storeState(
          db,
          {
            correlation_id: 1,
            version: 0,
          },
          -1,
        );

        let error;

        try {
          await Users.storeState(
            db,
            {
              correlation_id: 1,
              version: 1,
            },
            1,
          );
        } catch (err) {
          error = err;
        }

        expect(error).toBeInstanceOf(Error);
        expect(error).toHaveProperty('message');
        expect(error.message).toContain('E11000');
      });
    });
  });
});
