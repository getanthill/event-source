import { EventSourcedFactory } from '../../../src/EventSourced';

import setup from '../../setup';

describe('EventSourced', () => {
  describe('static', () => {
    describe('getCollection', () => {
      let mongodb;
      let db;

      beforeAll(async () => {
        mongodb = await setup.initDb();
        db = mongodb.DB;
      });

      afterAll(async () => {
        await setup.teardownDb(mongodb);
      });

      it('returns a collection', () => {
        class Users extends EventSourcedFactory('users', () => null) {}

        const collection = Users.getCollection(db, 'test');

        expect(collection.constructor.name).toEqual('Collection');
      });
    });
  });
});
