import { EventSourcedFactory } from '../../../src/EventSourced';

import setup from '../../setup';

describe('EventSourced', () => {
  describe('static', () => {
    describe('getSnapshotsCollection', () => {
      let mongodb;
      let db;

      beforeAll(async () => {
        mongodb = await setup.initDb();
        db = mongodb.DB;
      });

      afterAll(async () => {
        await setup.teardownDb(mongodb);
      });

      it('returns the states collection', () => {
        class Users extends EventSourcedFactory('users', () => null) {}

        const collection = Users.getSnapshotsCollection(db);

        expect(collection).toHaveProperty('collectionName', 'users_snapshots');
      });
    });
  });
});
