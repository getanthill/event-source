import { EventSourcedFactory } from '../../../src/EventSourced';

import setup from '../../setup';

describe('EventSourced', () => {
  describe('static', () => {
    describe('getEvents', () => {
      class Users extends EventSourcedFactory('users', () => null) {}

      let mongodb;
      let db;

      beforeAll(async () => {
        mongodb = await setup.initDb();
        db = mongodb.DB;
      });

      beforeEach(async () => {
        await Promise.all([
          Users.getStatesCollection(db).deleteMany(),
          Users.getEventsCollection(db).deleteMany(),
          Users.getSnapshotsCollection(db).deleteMany(),
        ]);
      });

      afterAll(async () => {
        await setup.teardownDb(mongodb);
      });

      it('returns a cursor on events', () => {
        const cursor = Users.getEvents(db, '1', -1);

        expect(cursor.constructor.name).toEqual('FindCursor');
      });

      it('returns the list of events not reduced yet', async () => {
        await Users.getEventsCollection(db).insertMany([
          {
            correlation_id: '1',
            version: 0,
          },
          {
            correlation_id: '1',
            version: 1,
          },
        ]);

        const cursor = Users.getEvents(db, '1', 0);

        const events = await cursor.toArray();

        expect(events).toHaveProperty('length', 1);
        expect(events[0]).toHaveProperty('version', 1);
      });

      it('returns the entire list of events if the object state does not have a version', async () => {
        await Users.getEventsCollection(db).insertMany([
          {
            correlation_id: '1',
            version: 0,
          },
          {
            correlation_id: '1',
            version: 1,
          },
        ]);

        const cursor = Users.getEvents(db, '1', -1);

        const events = await cursor.toArray();

        expect(events).toHaveProperty('length', 2);
        expect(events[0]).toHaveProperty('version', 0);
        expect(events[1]).toHaveProperty('version', 1);
      });
    });
  });
});
