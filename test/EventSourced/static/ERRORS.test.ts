import * as ERRORS from '../../../src/errors';

import { EventSourcedFactory } from '../../../src/EventSourced';

describe('EventSourced', () => {
  describe('static', () => {
    describe('getCollection', () => {
      it('returns the list of ERRORS', () => {
        class Users extends EventSourcedFactory('users', () => null) {}

        expect(Users.ERRORS).toEqual(ERRORS);
      });
    });
  });
});
