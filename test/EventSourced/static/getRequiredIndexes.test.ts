import { EventSourcedFactory } from '../../../src/EventSourced';

describe('EventSourced', () => {
  describe('static', () => {
    describe('getRequiredIndexes', () => {
      it('returns the indexes for 3 collections', () => {
        class Users extends EventSourcedFactory('users', () => null) {}

        expect(Users.getRequiredIndexes()).toEqual({
          users: [
            [
              { correlation_id: 1 },
              { unique: true, name: 'correlation_id_unicity' },
            ],
            [
              {
                created_at: 1,
              },
              {
                name: 'created_at_1',
              },
            ],
            [
              {
                version: 1,
              },
              {
                name: 'version_1',
              },
            ],
          ],
          users_events: [
            [
              {
                created_at: 1,
              },
              {
                name: 'created_at_1',
              },
            ],
            [
              {
                version: 1,
                created_at: 1,
              },
              {
                name: 'version_1_created_at_1',
              },
            ],
            [
              {
                version: 1,
              },
              {
                name: 'version_1',
              },
            ],
            [
              { correlation_id: 1, version: 1 },
              { unique: true, name: 'correlation_id_version_unicity' },
            ],
          ],
          users_snapshots: [
            [
              { correlation_id: 1, version: 1 },
              { unique: true, name: 'correlation_id_version_unicity' },
            ],
          ],
        });
      });

      it('returns the indexes for 3 collections with a model configured with global version', () => {
        class Users extends EventSourcedFactory('users', () => null, {
          WITH_GLOBAL_VERSION: true,
        }) {}

        expect(Users.getRequiredIndexes()).toEqual({
          users: [
            [
              { correlation_id: 1 },
              { unique: true, name: 'correlation_id_unicity' },
            ],
            [
              {
                created_at: 1,
              },
              {
                name: 'created_at_1',
              },
            ],
            [
              {
                version: 1,
              },
              {
                name: 'version_1',
              },
            ],
          ],
          users_events: [
            [
              {
                created_at: 1,
              },
              {
                name: 'created_at_1',
              },
            ],
            [
              {
                version: 1,
                created_at: 1,
              },
              {
                name: 'version_1_created_at_1',
              },
            ],
            [{ version: 1 }, { unique: true, name: 'version_unicity' }],
          ],
          users_snapshots: [
            [
              { correlation_id: 1, version: 1 },
              { unique: true, name: 'correlation_id_version_unicity' },
            ],
          ],
        });
      });
    });
  });
});
