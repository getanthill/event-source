import { AssertionError } from 'assert';

const logger = require('chpr-logger');
import setup from '../../setup';

import { EventSourcedFactory } from '../../../src/EventSourced';

describe.skip('[bench] EventSourced', () => {
  const INITIAL_STATE = {
    name: 'Standard',
    quantity: 10,
  };

  const reducer = (state, event) => {
    if (event.type === 'CREATED') {
      return {
        ...INITIAL_STATE,
        name: event.name || INITIAL_STATE.name,
        quantity: event.quantity || INITIAL_STATE.quantity,
      };
    }

    if (event.type === 'NAME_UPDATED') {
      return {
        ...state,
        name: event.name,
      };
    }

    if (event.type === 'ROOMS_BOOKED') {
      return {
        ...state,
        quantity: state.quantity - event.quantity,
      };
    }

    if (event.type === 'IS_READONLY') {
      return {
        ...state,
        is_readonly: event.is_readonly,
      };
    }

    return state;
  };

  class Rooms extends EventSourcedFactory('rooms', reducer, {
    CORRELATION_FIELD: 'room_id',
    WITH_BLOCKCHAIN_HASH: true,
  }) {
    /**
     * Create a new room
     * @param {string} name The room name
     * @param {number} quantity Room quantity
     * @return {Promise<object>} Updated state
     */
    create(name, quantity = 5) {
      return this.handle(
        () => [
          {
            type: 'CREATED',
            name,
            quantity,
          },
        ],
        () => null,
      );
    }

    book(
      quantity = 1,
      withRetry = false,
      retryDuration,
      storeStateErrorHandler,
      imperativeVersion?,
    ) {
      const handler = (state) => {
        if (state.quantity - quantity < 0) {
          throw new Error('No more room');
        }

        return [
          {
            type: 'ROOMS_BOOKED',
            quantity,
          },
        ];
      };

      if (withRetry === true) {
        return this.handleWithRetry(
          handler,
          retryDuration,
          storeStateErrorHandler,
          imperativeVersion,
        );
      }

      return this.handle(handler, storeStateErrorHandler, imperativeVersion);
    }
  }

  let mongodb;
  let db;

  beforeAll(async () => {
    mongodb = await setup.initDb();
    db = mongodb.DB;
  });

  beforeEach(async () => {
    await Promise.all([
      Rooms.getStatesCollection(db).deleteMany({}),
      Rooms.getEventsCollection(db).deleteMany({}),
      Rooms.getSnapshotsCollection(db).deleteMany({}),
    ]);

    await Rooms.initIndexes(db);
  });

  afterAll(async () => {
    await setup.teardownDb(mongodb);
  });

  afterEach(async () => {
    jest.resetAllMocks();
  });

  it('allows to create 1000 rooms in less than 2.5 seconds', async () => {
    const tic = Date.now();

    await Promise.all(
      new Array(1000).fill(1).map(() => {
        const room = new Rooms({ db, logger }, Math.floor(setup.random() * 1e9));

        return room.create('King', 5);
      }),
    );

    const tac = Date.now();

    expect(tac - tic).toBeLessThanOrEqual(2500);
  }, 30000);
});
