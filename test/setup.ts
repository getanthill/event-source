import { MongoDbConnector } from '@getanthill/mongodb-connector';
import crypto from 'crypto';

async function setup() {
  console.log('Global Setup');

  global.mongodb = await setup.initDb();
}

setup.initDb = async function initDb() {
  const DB_HOST = process.env.DB_HOST || 'localhost';
  const DB_PORT = process.env.DB_PORT || '27017';
  const DB_NAME = process.env.DB_NAME || 'test';
  const mongodb = new MongoDbConnector({
    ensureIndexInBackground: true,
    databases: [
      {
        name: DB_NAME,
        url: `mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`,
      },
    ],
  });

  await mongodb.connect();

  mongodb.DB_HOST = DB_HOST;
  mongodb.DB_PORT = DB_PORT;
  mongodb.DB_NAME = DB_NAME;
  mongodb.DB = mongodb.db(DB_NAME);

  await mongodb.DB.dropDatabase();

  return mongodb;
};

setup.teardownDb = async function teardownDb(mongodb) {
  await mongodb.DB.dropDatabase();
  await mongodb.disconnect();
};

setup.random = function random() {
  return Array.from(crypto.randomBytes(1))[0] / 256;
};

export default setup;
