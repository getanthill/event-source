import { AssertionError } from 'assert';

import { Snapshots } from '../../../src/Snapshots';

import setup from '../../setup';

describe('Events', () => {
  describe('static', () => {
    describe('getSnapshots', () => {
      let mongodb;
      let db;
      let collection;

      beforeAll(async () => {
        mongodb = await setup.initDb();
        db = mongodb.DB;
        collection = db.collection('snapshots');
      });

      beforeEach(async () => {
        await collection.deleteMany();
      });

      afterAll(async () => {
        await setup.teardownDb(mongodb);
      });

      it('returns the exact snapshot version if one match', async () => {
        await collection.insertMany([
          {
            correlation_id: '1',
            version: 0,
          },
          {
            correlation_id: '1',
            version: 1,
          },
        ]);

        const snapshot = await Snapshots.getSnapshot(
          collection,
          'correlation_id',
          '1',
          0,
        );

        expect(snapshot).toEqual({
          correlation_id: '1',
          version: 0,
        });
      });

      it('returns the last available snapshot from a requested version', async () => {
        await collection.insertMany([
          {
            correlation_id: '1',
            version: 0,
          },
          {
            correlation_id: '1',
            version: 1,
          },
        ]);

        const snapshot = await Snapshots.getSnapshot(
          collection,
          'correlation_id',
          '1',
          10,
        );

        expect(snapshot).toEqual({
          correlation_id: '1',
          version: 1,
        });
      });

      it('throws an error if no correlation field is given', () => {
        let error;

        try {
          Snapshots.getSnapshot(collection);
        } catch (err) {
          error = err;
        }

        expect(error).toBeInstanceOf(AssertionError);
        expect(error).toHaveProperty(
          'message',
          '[Snapshots#getSnapshot] Missing correlationField',
        );
      });

      it('throws an error if no correlation id is given', () => {
        let error;

        try {
          Snapshots.getSnapshot(collection, 'correlation_id');
        } catch (err) {
          error = err;
        }

        expect(error).toBeInstanceOf(AssertionError);
        expect(error).toHaveProperty(
          'message',
          '[Snapshots#getSnapshot] Missing correlationId',
        );
      });

      it('throws an error if the version is not a number', () => {
        let error;

        try {
          Snapshots.getSnapshot(collection, 'correlation_id', '1');
        } catch (err) {
          error = err;
        }

        expect(error).toBeInstanceOf(AssertionError);
        expect(error).toHaveProperty(
          'message',
          '[Snapshots#getSnapshot] Invalid version (must be a number)',
        );
      });
    });
  });
});
