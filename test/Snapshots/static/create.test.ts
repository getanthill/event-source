import { AssertionError } from 'assert';

import { Snapshots } from '../../../src/Snapshots';

import setup from '../../setup';

describe('Snapshots', () => {
  describe('static', () => {
    describe('create', () => {
      let mongodb;
      let db;
      let collection;

      beforeAll(async () => {
        mongodb = await setup.initDb();
        db = mongodb.DB;
        collection = db.collection('snapshots');
      });

      beforeEach(async () => {
        await Promise.all([
          collection.deleteMany(),
          Snapshots.initIndexes(collection, 'correlation_id'),
        ]);
      });

      afterAll(async () => {
        await setup.teardownDb(mongodb);
      });

      it('stores events into the collection successfully', async () => {
        await Snapshots.create(collection, 'correlation_id', {
          correlation_id: 1,
          version: 0,
        });

        const snapshot = await Snapshots.getSnapshot(
          collection,
          'correlation_id',
          1,
          0,
        );

        expect(snapshot).toEqual({
          correlation_id: 1,
          version: 0,
        });
      });

      it('does not throw an error if trying to insert 2 times a snapshot with the same correlation id and version', async () => {
        await Snapshots.create(collection, 'correlation_id', {
          correlation_id: 1,
          version: 0,
        });

        await Snapshots.create(collection, 'correlation_id', {
          correlation_id: 1,
          version: 0,
          invalid: true,
        });

        const snapshot = await Snapshots.getSnapshot(
          collection,
          'correlation_id',
          1,
          0,
        );

        expect(snapshot).toEqual({
          correlation_id: 1,
          version: 0,
        });
      });

      it('throws an error if state is null', async () => {
        let error;

        try {
          await Snapshots.create(collection, 'correlation_id', null);
        } catch (err) {
          error = err;
        }

        expect(error).toBeInstanceOf(AssertionError);
        expect(error.message).toEqual(
          '[Snapshots#create] State can not be null',
        );
      });
    });
  });
});
