async function teardown() {
  console.log('Global Teardown');

  await global.mongodb.disconnect();
}

export default teardown;
