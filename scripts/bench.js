const memwatch = require('@airbnb/node-memwatch');
const { MongoDbConnector, ObjectId } = require('@getanthill/mongodb-connector');

const { EventSourcedFactory } = require('../dist/bundle.cjs');
const fs = require('fs');

const RESULTS_PATH = __dirname + '/bench_results.json';
const current = require(RESULTS_PATH);

const INITIAL_STATE = {
  name: 'Standard',
  quantity: 10,
};

const reducer = (state, event) => {
  if (event.type === 'CREATED') {
    return {
      ...INITIAL_STATE,
      name: event.name || INITIAL_STATE.name,
      quantity: event.quantity || INITIAL_STATE.quantity,
    };
  }

  if (event.type === 'NAME_UPDATED') {
    return {
      ...state,
      name: event.name,
    };
  }

  if (event.type === 'ROOMS_BOOKED') {
    return {
      ...state,
      quantity: state.quantity - event.quantity,
    };
  }

  return state;
};

class Rooms extends EventSourcedFactory('rooms', reducer, {
  CORRELATION_FIELD: 'room_id',
}) {
  /**
   * Create a new room
   * @param {string} name The room name
   * @param {number} quantity Room quantity
   * @return {Promise<object>} Updated state
   */
  create(name, quantity = 5) {
    return this.handle(() => [
      {
        type: 'CREATED',
        name,
        quantity,
      },
    ]);
  }

  book(
    quantity = 1,
    withRetry = false,
    retryDuration,
    storeStateErrorHandler,
    imperativeVersion,
  ) {
    const handler = (state) => {
      if (state.quantity - quantity < 0) {
        throw new Error('No more room');
      }

      return [
        {
          type: 'ROOMS_BOOKED',
          quantity,
        },
      ];
    };

    if (withRetry === true) {
      return this.handleWithRetry(
        handler,
        retryDuration,
        storeStateErrorHandler,
        imperativeVersion,
      );
    }

    return this.handle(handler, storeStateErrorHandler, imperativeVersion);
  }
}

function uuid() {
  return new ObjectId().toHexString();
}

function time(from) {
  const hrTime = process.hrtime(from);

  return (hrTime[0] * 1000000000 + hrTime[1]) / 1000000;
}

function average(arr) {
  return Math.round((arr.reduce((a, b) => a + b, 0) / arr.length) * 100) / 100;
}

function median(values) {
  if (values.length === 0) throw new Error('No inputs');

  values.sort(function (a, b) {
    return a - b;
  });

  var half = Math.floor(values.length / 2);

  if (values.length % 2) return values[half];

  return (values[half - 1] + values[half]) / 2.0;
}

async function flow(db, scenario) {
  const roomId = uuid();

  if (scenario === 'mongo') {
    await db.collection('rooms').insertOne({
      room_id: roomId,
      quantity: 5,
    });

    return;
  }

  if (scenario === 'create') {
    const room = new Rooms({ db }, roomId);
    await room.create('King', 5);

    return;
  }

  if (scenario === 'update') {
    const room = new Rooms({ db }, _entityId);
    await room.book(1, true, 5000);

    return;
  }
}

let _entityId;
let _entity;

async function main(
  scenario,
  iterations = 1,
  parallel = 1,
  progress = 100,
  mustSave = true,
) {
  console.log(`[bench]`, {
    scenario,
    iterations,
    parallel,
    progress,
  });

  const mongodb = new MongoDbConnector({
    ensureIndexInBackground: true,
    databases: [
      {
        name: 'bench',
        url: `mongodb://localhost:27017/datastore_${(
          Math.random() * 1e10
        ).toFixed(0)}`,
      },
    ],
  });

  await mongodb.connect();

  const db = mongodb.db('bench');

  await Rooms.initIndexes(db);

  _entityId = uuid();
  _entity = new Rooms({ db }, _entityId);

  await _entity.create('King', 1e10);

  const times = [];
  let stepTimes = [];

  const hd = new memwatch.HeapDiff();

  for (let i = 0; i < iterations; i++) {
    const tic = process.hrtime();
    await Promise.all(
      new Array(parallel).fill(1).map(() => flow(db, scenario)),
    );

    const tac = time(tic);
    times.push(tac);
    stepTimes.push(tac);

    if (i % progress === 0) {
      console.log(
        `${i} / ${((100 * i) / iterations).toFixed(2)}%`,
        (Math.min(...stepTimes) / parallel).toFixed(3),
        (average(stepTimes) / parallel).toFixed(3),
        (median(stepTimes) / parallel).toFixed(3),
        (Math.max(...stepTimes) / parallel).toFixed(3),
      );
      stepTimes = [];
    }
  }

  const heapDiff = hd.end();

  const s = `${scenario}-${iterations}-${parallel}`;
  const results = {
    ts: new Date(),
    memory_in_kb: heapDiff.change.size_bytes / 1024,
    timings_in_ms: {
      min: Math.min(...times) / parallel,
      average: average(times) / parallel,
      median: median(times) / parallel,
      max: Math.max(...times) / parallel,
    },
  };

  console.log('[bench] Results', results);

  let diff;
  let variation;
  if (!!current[s]) {
    diff = {
      memory_in_kb: results.memory_in_kb - current[s].last.memory_in_kb,
      timings_in_ms: {
        min: results.timings_in_ms.min - current[s].last.timings_in_ms.min,
        average:
          results.timings_in_ms.average - current[s].last.timings_in_ms.average,
        median:
          results.timings_in_ms.median - current[s].last.timings_in_ms.median,
        max: results.timings_in_ms.max - current[s].last.timings_in_ms.max,
      },
    };

    variation = {
      memory_in_kb:
        Math.round((100 * diff.memory_in_kb) / current[s].last.memory_in_kb) /
        100,
      timings_in_ms: {
        min:
          Math.round(
            (100 * diff.timings_in_ms.min) / current[s].last.timings_in_ms.min,
          ) / 100,
        average:
          Math.round(
            (100 * diff.timings_in_ms.average) /
              current[s].last.timings_in_ms.average,
          ) / 100,
        median:
          Math.round(
            (100 * diff.timings_in_ms.median) /
              current[s].last.timings_in_ms.median,
          ) / 100,
        max:
          Math.round(
            (100 * diff.timings_in_ms.max) / current[s].last.timings_in_ms.max,
          ) / 100,
      },
    };

    console.log('[bench] Diff', diff);
    console.log('[bench] Variation (%)', variation);
  }

  await mongodb.db('bench').dropDatabase();

  await mongodb.disconnect();

  if (mustSave === 'true') {
    fs.writeFileSync(
      RESULTS_PATH,
      JSON.stringify(
        {
          ...current,
          [s]: {
            last: results,
            previous: !!current[s] && current[s].last,
            diff,
            variation,
          },
        },
        null,
        2,
      ),
    );
  }
}

main(...process.argv.slice(2))
  .then(() => {
    console.log('[bench] Ended successfully');
  })
  .catch((err) => {
    console.error('[bench] Error', err);

    process.exit(1);
  });
